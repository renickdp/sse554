﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataRecorderServer;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DataRecorderServerTests
{
    [TestClass]
    public class ClientServerTests
    {
        private static string logFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\DataRecorderServer\";
        private static string logFile = Path.Combine(logFolder, "log.zip");
        private static string logFile2 = Path.Combine(logFolder, "log.txt");

        [TestMethod]
        public void ClientServerTest_ReadWrite()
        {
            int port = 9000;
            string expected = "Server-Client Unit Test Value";
            byte[] buffer = Encoding.ASCII.GetBytes(expected);

            var ss = new SimpleServer(port);
            var sc = new SimpleClient(port);

            var di = new DirectoryInfo(logFolder);
            if (di.Exists)
                Directory.Delete(logFolder, true);

            Thread.Sleep(20);

            Task run = ss.RunServer();
            Thread.Sleep(10);
            sc.SendData(buffer);
            Thread.Sleep(10);
            ss.StopServer();
            run.Wait();
            
            Compressor.ConvertLog(logFile, logFile2);

            string contents;
            using (var fs = File.OpenRead(logFile2))
            using (var sr = new StreamReader(fs))
            {
                contents = sr.ReadLine();
            }

            Assert.AreEqual(expected, contents);
        }
    }
}
