﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataRecorderServer;
using System.Threading;

namespace DataRecorderServerTests
{
    [TestClass]
    public class CompressorTests
    {
        private static string logFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\DataRecorderServer\";
        private static string logFile = Path.Combine(logFolder, "log.zip");
        private static string logFile2 = Path.Combine(logFolder, "log.txt");

        [TestMethod]
        public void CompressorTest_SaveLog()
        {
            var c = new Compressor();

            var di = new DirectoryInfo(logFolder);
            if (di.Exists)
                Directory.Delete(logFolder, true);

            Thread.Sleep(10);

            var di2 = new DirectoryInfo(logFolder);
            Assert.IsFalse(di2.Exists);

            Thread.Sleep(10);

            c.SaveLogData("Unit Test1");

            Thread.Sleep(10);

            di2 = new DirectoryInfo(logFolder);
            Assert.IsTrue(di.Exists);

            var fi = new FileInfo(logFile);
            Assert.IsTrue(fi.Exists);
        }

        [TestMethod]
        public void CompressorTest_CheckLog()
        {
            var c = new Compressor();

            var di = new DirectoryInfo(logFolder);
            if (di.Exists)
                Directory.Delete(logFolder, true);

            Thread.Sleep(10);

            c.SaveLogData("Unit Test1");

            Thread.Sleep(10);

            Compressor.ConvertLog(logFile, logFile2);

            Thread.Sleep(10);

            string contents;
            using (var fs = File.OpenRead(logFile2))
            using(var sr = new StreamReader(fs))
            {
                contents = sr.ReadLine();
            }

            Assert.AreEqual("Unit Test1", contents);
        }
    }
}
