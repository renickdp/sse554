﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataRecorderServer;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DataRecorderServerTests
{
    [TestClass]
    public class PipeManagerTests
    {
        private static string logFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\DataRecorderServer\";
        private static string logFile = Path.Combine(logFolder, "log.zip");
        private static string logFile2 = Path.Combine(logFolder, "log.txt");

        [TestMethod]
        public void PipeManagerTest_ReadWrite()
        {
            string expected = "PipeManager Unit Test Value";
            byte[] buffer = Encoding.ASCII.GetBytes(expected);
            var pm = new PipeManager("TestPipe");

            var di = new DirectoryInfo(logFolder);
            if (di.Exists)
                Directory.Delete(logFolder, true);

            Thread.Sleep(20);
            
            Task run = pm.RunServer();
            pm.SendData(buffer);
            pm.StopServer();
            run.Wait();

            Thread.Sleep(20);

            Compressor.ConvertLog(logFile, logFile2);

            string contents;
            using (var fs = File.OpenRead(logFile2))
            using (var sr = new StreamReader(fs))
            {
                contents = sr.ReadLine();
            }

            Assert.AreEqual(expected, contents);
        }
    }
}
