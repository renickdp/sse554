﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DataRecorderServer
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                switch (args[0])
                {
                    case "s":
                        Console.WriteLine("Running as tcp server");
                        runAsTpcServer();
                        break;

                    case "c":
                        Console.WriteLine("Running as tcp client");
                        runAsTcpClient();
                        break;

                    case "ps":
                        Console.WriteLine("Running as pipe sever");
                        runAsPipeServer();
                        break;

                    case "pc":
                        Console.WriteLine("Running as pipe client");
                        runAsPipeClient();
                        break;
                }                
            }
            else
            {
                Console.WriteLine("Running as tcp server");
                runAsTpcServer();
            }
        }

        private static void runAsTpcServer()
        {
            string input;
            var server = new SimpleServer(1997, Console.WriteLine);
            Task runner = server.RunServer();

            do
            {
                Console.WriteLine("Type 'e' to quit server:");
                input = Console.ReadLine();
            } while (input != "e");
            server.StopServer();
            Task.WaitAll(runner);
        }

        private static void runAsTcpClient()
        {
            var client = new SimpleClient(1997);
            byte[] buffer = Encoding.ASCII.GetBytes("Test TCP Data");

            client.SendData(buffer);
        }

        private static void runAsPipeServer()
        {
            string input;
            var server = new PipeManager("PosSystemPipe", Console.WriteLine);
            Task runner = server.RunServer();

            do
            {
                Console.WriteLine("Type 'e' to quit server:");
                input = Console.ReadLine();
            } while (input != "e");
            server.StopServer();
            Task.WaitAll(runner);
        }

        private static void runAsPipeClient()
        {
            var client = new PipeManager("PosSystemPipe", Console.WriteLine);
            byte[] buffer = Encoding.ASCII.GetBytes("Test Pipe Data");

            client.SendData(buffer);
        }
    }
}
