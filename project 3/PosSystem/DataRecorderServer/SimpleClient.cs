﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace DataRecorderServer
{
    public class SimpleClient
    {
        private string address;
        private int port;
        
        public SimpleClient()
        {
            this.address = "localhost";
            port = 1997;
        }

        public SimpleClient(int port)
        {
            this.address = "localhost";
            this.port = port;
        }

        public void SendData(byte[] buffer)
        {
            TcpClient client = new TcpClient();

            try
            {
                client.Connect("localhost", port);

                using (NetworkStream nStream = client.GetStream())
                {
                    Console.WriteLine("Writting Data");
                    nStream.Write(buffer, 0, buffer.Length);
                    Console.WriteLine("Flushing");
                    nStream.Flush();
                    Console.WriteLine("Ending");
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                client.Close();
            }
        }
    }
}
