﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;
using System.Threading;
using System.IO;

namespace DataRecorderServer
{
    public class PipeManager
    {
        private string name;
        private StatusReporter reporter;
        private CancellationTokenSource cancelToken;
        private Compressor compressor;

        public PipeManager()
        {
            name = "";
            reporter = Console.WriteLine;
            cancelToken = new CancellationTokenSource();
            compressor = new Compressor();
        }

        public PipeManager(string name)
        {
            this.name = name;
            reporter = Console.WriteLine;
            cancelToken = new CancellationTokenSource();
            compressor = new Compressor();
        }

        public PipeManager(string name, StatusReporter reporter)
        {
            this.name = name;
            this.reporter = reporter;
            cancelToken = new CancellationTokenSource();
            compressor = new Compressor();
        }

        public void SendData(byte[] buffer)
        {
            using(var writer = new NamedPipeClientStream(name))
            {
                writer.Connect(1000);
                
                writer.Write(buffer, 0, buffer.Length);
                writer.Flush();           
            }
        }

        public void StopServer()
        {
            cancelToken.Cancel();
        }
        
        public Task RunServer()
        {
            return Task.Run(() =>
            {
                try
                {
                    while (!cancelToken.IsCancellationRequested)
                    {
                        using (var reader = new NamedPipeServerStream(name))
                        {
                            readFromPipe(reader);
                        }
                    }
                }
                catch(Exception ex)
                {
                    reporter(ex.Message);
                }
            });
        }

        private void readFromPipe(NamedPipeServerStream reader)
        {
            try
            {
                reader.WaitForConnection();
                
                using (var sr = new StreamReader(reader))
                {
                    string request = sr.ReadLine();
                    reporter(String.Format("Recieved {0}", request));
                    compressor.SaveLogData(request);
                }
            }
            catch(Exception ex)
            {
                reporter(ex.Message);
            }
        }
    }
}
