﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataRecorderServer
{
    public class DataManager
    {
        private double value;

        public DataManager()
        {
            this.value = 0;
        }

        public void AddData(string data)
        {
            double newValue;

            if(Double.TryParse(data, out newValue))
            {
                value += newValue;
            }
        }

        public void SaveData()
        {
            
        }
    }
}
