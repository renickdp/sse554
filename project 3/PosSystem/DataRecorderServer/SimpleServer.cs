﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace DataRecorderServer
{
    public delegate void StatusReporter(string message);

    public class SimpleServer
    {
        private IPAddress address;
        private int port;
        private StatusReporter reporter;
        private CancellationTokenSource cancelToken;
        private Compressor compressor;

        public SimpleServer()
        {
            address = IPAddress.Any;
            port = 1997;
            reporter = Console.WriteLine;
            cancelToken = new CancellationTokenSource();
            compressor = new Compressor();
        }

        public SimpleServer(int port)
        {
            address = IPAddress.Any;
            this.port = port;
            this.reporter = Console.WriteLine;
            cancelToken = new CancellationTokenSource();
            compressor = new Compressor();
        }

        public SimpleServer(int port, StatusReporter reporter)
        {
            address = IPAddress.Any;
            this.port = port;
            this.reporter = reporter;
            cancelToken = new CancellationTokenSource();
            compressor = new Compressor();
        }

        public void StopServer()
        {
            cancelToken.Cancel();
        }

        public Task RunServer()
        {
            return Task.Run(() =>
            {
                var listener = new TcpListener(address, port);

                try
                {
                    reporter(String.Format("Listening to requests on port {0}", port));
                    listener.Start();                    

                    while(!cancelToken.IsCancellationRequested)
                    {
                        IAsyncResult ar = listener.BeginAcceptTcpClient(RunClientRequest, listener);
                        ar.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(2), false);  
                    }
                }
                catch(Exception ex)
                {
                    reporter(ex.Message);
                }
                finally
                {
                    listener.Stop();
                }
            });
        }

        private void RunClientRequest(IAsyncResult ar)
        {
            TcpListener listener = (TcpListener)ar.AsyncState;

            if(listener.Server.IsBound)
            {
                try
                {
                    TcpClient client = listener.EndAcceptTcpClient(ar);
                    using (client)
                    {
                        reporter("Client has connected");
                        using (NetworkStream nStream = client.GetStream())
                        {
                            byte[] readBuffer = new byte[1024];
                            int read = nStream.Read(readBuffer, 0, readBuffer.Length);

                            string request = Encoding.ASCII.GetString(readBuffer, 0, read);
                            reporter(String.Format("Recieved {0}", request));
                            compressor.SaveLogData(request);
                        }
                    }
                }
                catch (Exception ex)
                {
                    reporter(ex.Message);
                }
                reporter("Client disconnected");
            }
        }
    }
}
