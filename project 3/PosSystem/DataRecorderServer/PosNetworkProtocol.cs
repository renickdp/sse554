﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataRecorderServer
{
    public static class PosNetworkProtocol
    {
        //public const string SESSION_ID = "ID";
        //public const string COMMAND_OPEN = "OPEN";
        public const string COMMAND_SAVE = "SAVE";

        public const string STATUS_SUCCESS = "SUCCESS";
        public const string STATUS_ERROR = "ERROR";
        public const string STATUS_CLOSED = "CLOSED";
        public const string STATUS_TIMEOUT = "TIMEOUT";

        public const string SEPARATOR = "::";

        public static readonly TimeSpan SessionTimeout = TimeSpan.FromMinutes(2);
    }
}
