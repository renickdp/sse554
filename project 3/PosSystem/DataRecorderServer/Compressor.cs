﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;

namespace DataRecorderServer
{
    public class Compressor
    {
        string logFolder;
        string logFile;

        public Compressor()
        {
            logFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\DataRecorderServer\";
            logFile = Path.Combine(logFolder, "log.zip");
        }

        public void SaveLogData(string line)
        {
            checkLogFolder();

            string previous = DecompressLog();
            string log = (previous == "") ? line : previous + Environment.NewLine + line;

            using (FileStream outstream = File.Open(logFile, FileMode.Create))
            {
                using (var compressStream = new DeflateStream(outstream, CompressionMode.Compress))
                {
                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(log);
                    compressStream.Write(buffer, 0, buffer.Length);
                }
            }

        }

        private void checkLogFolder()
        {
            var di = new DirectoryInfo(logFolder);
            if(!di.Exists)
                Directory.CreateDirectory(logFolder);
        }

        private string DecompressLog()
        {
            string log = "";

            if (File.Exists(logFile))
            {
                using (var inStream = File.OpenRead(logFile))
                {
                    using (var outStream = new MemoryStream())
                    using (var compressStream = new DeflateStream(inStream, CompressionMode.Decompress))
                    {
                        compressStream.CopyTo(outStream);
                        outStream.Seek(0, SeekOrigin.Begin);
                        using (var reader = new StreamReader(outStream, Encoding.UTF8))
                        {
                            log = reader.ReadToEnd();
                        }
                    }
                }
            }

            return log;
        }

        public static void ConvertLog(string inPath, string outPath)
        {
            using (FileStream inStream = File.OpenRead(inPath))
            using (FileStream outStream = File.OpenWrite(outPath))
            {
                using (var compressStream = new DeflateStream(inStream, CompressionMode.Decompress))
                {
                    compressStream.CopyTo(outStream);
                }
            }
        } 
    }
}
