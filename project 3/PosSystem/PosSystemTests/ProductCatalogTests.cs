﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PosSystem;
using System.IO;
using System.Threading;

namespace PosSystemTests
{
    [TestClass]
    public class ProductCatalogTests
    {
        private const string testXmlCatalog = "<catalog><item><id>1</id><name>UT1</name><price>10</price></item><item><id>2</id><name>UT2</name><price>100</price></item></catalog>";
        private static string catalogFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PosSystem\";
        private static string catalogFile = Path.Combine(catalogFolder, "unit_test_catalog.xml");

        [TestMethod]
        public void ProductCatalogTest_LoadSample()
        {
            var pc = new ProductCatalog();

            pc.LoadSample();

            Assert.AreEqual(1, pc.Catalog[0].Id);
            Assert.AreEqual("Item 1", pc.Catalog[0].Name);
            Assert.AreEqual(100, pc.Catalog[0].Value);
            Assert.AreEqual(10, pc.Catalog.Count);
        }

        [TestMethod]
        public void ProductCatalogTest_Lookup()
        {
            var pc = new ProductCatalog();
           
            pc.LoadSample();
            var lookup = pc.Lookup(9);

            Assert.AreEqual(9, lookup.Id);
            Assert.AreEqual("Item 9", lookup.Name);
            Assert.AreEqual(10, lookup.Value);
        }

        [TestMethod]
        public void ProductCatalogTest_LoadFromXml()
        {
            var pc = new ProductCatalog();

            var di = new DirectoryInfo(catalogFolder);
            if (!di.Exists)
                Directory.CreateDirectory(catalogFolder);
            
            var fi = new FileInfo(catalogFile);
            if (!fi.Exists)
            {
                using(var fs = File.OpenWrite(catalogFile))
                using(var sw = new StreamWriter(fs))
                {
                    sw.Write(testXmlCatalog);
                    Thread.Sleep(20);
                }
            }

            pc.LoadFromXml(catalogFile);

            Assert.AreEqual(1, pc.Catalog[0].Id);
            Assert.AreEqual("UT1", pc.Catalog[0].Name);
            Assert.AreEqual(10, pc.Catalog[0].Value);
            Assert.AreEqual(2, pc.Catalog[1].Id);
            Assert.AreEqual("UT2", pc.Catalog[1].Name);
            Assert.AreEqual(100, pc.Catalog[1].Value);
        }
    }
}
