﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PosSystem;

namespace PosSystemTests
{
    [TestClass]
    public class ProductTests
    {
        [TestMethod]
        public void ProductTest_Test1()
        {
            int exId = 1000;
            string exName = "Test Name";
            int exValue = 90000;
            string exString = exId + " - " + exName;

            var p = new Product(exId, exName, exValue);

            Assert.AreEqual(exId, p.Id);
            Assert.AreEqual(exName, p.Name);
            Assert.AreEqual(exValue, p.Value);
            Assert.AreEqual(exString, p.ToString());
        }
    }
}
