﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using PosSystem;
using System.Security.Cryptography;

namespace PosSystemTests
{
    [TestClass]
    public class EncryptionTests
    {
        [TestMethod]
        public void EncryptionTest()
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] encryptedtext;
            var Cardenc = ByteConverter.GetBytes("1337");
            encryptedtext = Asymmetric.RSAEncrypt(Cardenc, RSA.ExportParameters(false), false);

            Assert.AreEqual(128, encryptedtext.Length);

        }

        [TestMethod]
        public void DencryptionTest()
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] encryptedtext;

            var Cardenc = ByteConverter.GetBytes("1337");
            encryptedtext = Asymmetric.RSAEncrypt(Cardenc, RSA.ExportParameters(false), false);

            byte[] decryptedtex = Asymmetric.RSADecrypt(encryptedtext, RSA.ExportParameters(true), false);
            var decrval = ByteConverter.GetString(decryptedtex);

            Assert.AreEqual("1337", decrval);

        }










    }
}
