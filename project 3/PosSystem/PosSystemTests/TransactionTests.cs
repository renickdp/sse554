﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PosSystem;

namespace PosSystemTests
{
    [TestClass]
    public class TransactionTests
    {
        [TestMethod]
        public void TransactionTest_Add()
        {
            var p = new Product(5, "I5", 500);
            var t = new Transaction();

            t.Add(p);

            Assert.AreEqual(5, t.Items[0].Id);
            Assert.AreEqual("I5", t.Items[0].Name);
            Assert.AreEqual(500, t.Items[0].Value);
        }

        [TestMethod]
        public void TransactionTest_Clear()
        {
            var p1 = new Product(5, "I5", 500);
            var p2 = new Product(2, "I2", 1500);
            var t = new Transaction();

            t.Add(p1);
            t.Add(p2);

            Assert.AreEqual(2, t.Items.Count);

            t.Clear();

            Assert.AreEqual(0, t.Items.Count);
        }

        [TestMethod]
        public void TransactionTest_GetTotal()
        {
            var p1 = new Product(5, "I5", 500);
            var p2 = new Product(2, "I2", 1500);
            var t = new Transaction();

            t.Add(p1);
            t.Add(p2);

            Assert.AreEqual(2000, t.GetTotal());
        }

        [TestMethod]
        public void TransactionTest_Clone()
        {
            var p1 = new Product(5, "I5", 500);
            var p2 = new Product(2, "I2", 1500);
            var t1 = new Transaction();

            t1.Add(p1);
            t1.Add(p2);

            var t2 = (Transaction)t1.Clone();

            Assert.AreNotEqual(t1, t2);
            Assert.AreEqual(t1.Items[0], t2.Items[0]);
            Assert.AreEqual(t1.Items[1], t2.Items[1]);
        }

        [TestMethod]
        public void TransactionTest_ToString()
        {
            string ex = "$2,000.00, 5 - I5, 2 - I2";
            var p1 = new Product(5, "I5", 500);
            var p2 = new Product(2, "I2", 1500);
            var t1 = new Transaction();

            t1.Add(p1);
            t1.Add(p2);

            Assert.AreEqual(ex, t1.ToString());
        }
    }
}
