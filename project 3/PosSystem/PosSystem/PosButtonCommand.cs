﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PosSystem
{
    public class PosButtonCommand : ICommand
    {
        Action<object> delegateMethod;
        bool executing;

        public bool CanExecute(object parameter)
        {
            if (executing == true) return false;

            if (delegateMethod != null)
                return true;
            else
                return false;
        }

        public void Execute(object parameter)
        {
            executing = true;
            OnCanExecuteChanged();

            delegateMethod(parameter);

            executing = false;
            OnCanExecuteChanged();
        }

        public PosButtonCommand(Action<object> delegateMethod)
        {
            executing = false;
            this.delegateMethod = delegateMethod;
        }

        #region ICommand implementation
        public event EventHandler CanExecuteChanged;

        private void OnCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if(handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        #endregion
    }
}
