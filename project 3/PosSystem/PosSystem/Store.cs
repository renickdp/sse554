﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem
{
    public class Store
    {
        private SalesLedger ledger;
        private Transaction currentSale;
        private ProductCatalog Catalog;

        private string file = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PosSystem\catalog.xml";

        public double SaleTotal
        {
            get { return currentSale.GetTotal(); }
        }

        public double SaleTax
        {
            get { return currentSale.GetTotal() * 0.07; }
        }

        public double GrandTotal
        {
            get { return (SaleTotal + SaleTax); }
        }

        public Store()
        {
            ledger = new SalesLedger();
            currentSale = new Transaction();
            Catalog = new ProductCatalog();
        }

        public void CancelSale()
        {
            currentSale.Clear();
        }

        public void FinishSale()
        {
            ledger.Add(currentSale);
            currentSale.Clear();
        }

        public void LoadSampleProducts()
        {
            Catalog.LoadSample();
        }

        public void LoadProductsFromXml(string path)
        {
            Catalog.LoadFromXml(file);
        }

        public void AddItemToSale(int id)
        {
            currentSale.Add(Catalog.Lookup(id));
        }

        public IEnumerable<Product> GetSaleListing()
        {
            return currentSale.Items;
        }

        public IEnumerable<Product> GetProductCatalog()
        {
            return Catalog.Catalog;
        }

        public void SetSaveSystem(SaveSystem saver)
        {
            ledger.Saver = saver;
        }
    }
}
