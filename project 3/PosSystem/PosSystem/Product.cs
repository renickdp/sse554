﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem
{
    public class Product
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        private double value;
        public double Value
        {
            get { return value; }
        }

        public Product(int id, string name, double value)
        {
            this.id = id;
            this.name = name;
            this.value = value;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", id, name);
        }
    }
}
