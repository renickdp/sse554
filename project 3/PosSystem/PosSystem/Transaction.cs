﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosSystem
{
    public class Transaction : ICloneable
    {
        private List<Product> items;
        public List<Product> Items
        {
            get { return items; }
        }

        public Transaction()
        {
            items = new List<Product>();
        }

        protected Transaction(Transaction t)
        {
            items = new List<Product>();
            foreach(var i in t.items)
            {
                items.Add(i);
            }
        }

        public void Clear()
        {
            items.Clear();
        }

        public void Add(Product item)
        {
            items.Add(item);
        }    
   
        public object Clone()
        {
            return new Transaction(this);
        }

        public double GetTotal()
        {
            double total = 0;
            foreach (var i in items)
                total += i.Value;

            return total;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(String.Format("{0:c2}", GetTotal()));

            foreach (var i in Items)
                sb.Append(", " + i.ToString());

            return sb.ToString();
        }
    }
}
