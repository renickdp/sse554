﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;

namespace PosSystem
{
    public class PipeClient
    {
        private string name;

        public PipeClient()
        {
            name = "";
        }

        public PipeClient(string name)
        {
            this.name = name;
        }

        public void SendData(byte[] buffer)
        {
            using (var writer = new NamedPipeClientStream(name))
            {
                writer.Connect(1000);

                writer.Write(buffer, 0, buffer.Length);
                writer.Flush();
            }
        }
    }
}
