﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosSystem
{
    public partial class EditCatalogForm : Form
    {
        private List<Product> modifiedCatalog;
        private List<Product> originalCatalog;
        private PosManager manager;
        public EditCatalogForm(PosManager manager)
        {
            InitializeComponent();
            this.manager = manager;
            originalCatalog = (List<Product>)manager.Store.GetProductCatalog();
            modifiedCatalog = new List<Product>(originalCatalog);
            foreach (Product p in originalCatalog)
            {
                ListViewItem item = new ListViewItem(p.Name);
                item.SubItems.Add(p.Value.ToString());
                catalogListView.Items.Add(item);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            originalCatalog.Clear();
            originalCatalog.AddRange(modifiedCatalog);
            manager.refreshProductButtons();
            this.Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            addPanel.Visible = true;
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem selectedItem in catalogListView.SelectedItems)
            {
                modifiedCatalog.Remove(modifiedCatalog.Where(i => i.Name == selectedItem.Text).First());
                catalogListView.Items.Remove(selectedItem);
            }
        }

        private void newNameTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).Text = "";
        }

        private void newValueTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            ((TextBox)sender).Text = "";
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            modifiedCatalog.Add(new Product(modifiedCatalog.Last().Id+1, newNameTextBox.Text, Double.Parse(newValueTextBox.Text)));
            ListViewItem item = new ListViewItem(modifiedCatalog.Last().Name);
            item.SubItems.Add(modifiedCatalog.Last().Value.ToString());
            catalogListView.Items.Add(item);
            addPanel.Visible = false;
        }
    }
}
