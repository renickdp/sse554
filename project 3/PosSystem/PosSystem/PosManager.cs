﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using DataRecorderServer;

namespace PosSystem
{
    public class PosManager : INotifyPropertyChanged
    {
        #region Binding fields
        private PosButtonCommand cancelCommand;
        public PosButtonCommand CancelCommand
        {
            get { return cancelCommand; }
            set
            {
                cancelCommand = value;
                OnPropertyChanged("CancelCommand");
            }
        }

        private PosButtonCommand payCommand;
        public PosButtonCommand PayCommand
        {
            get { return payCommand; }
            set
            {
                payCommand = value;
                OnPropertyChanged("PayCommand");
            }
        }



        private ObservableCollection<PosButtonViewModel> saleButtons;
        public ObservableCollection<PosButtonViewModel> SaleButtons
        {
            get { return saleButtons; }
            set
            {
                saleButtons = value;
                OnPropertyChanged("SaleButtons");
            }
        }

        private ObservableCollection<SaleItemViewModel> currentSaleList;
        public ObservableCollection<SaleItemViewModel> CurrentSaleList
        {
            get { return currentSaleList; }
            set
            {
                currentSaleList = value;
                OnPropertyChanged("CurrentSaleList");
            }
        }

        private string total;
        public string Total
        {
            get { return total; }
            set
            {
                total = value;
                OnPropertyChanged("Total");
            }
        }

        private string tax;
        public string Tax
        {
            get { return tax; }
            set
            {
                tax = value;
                OnPropertyChanged("Tax");
            }
        }

        private string grandtotal;
        public string GrandTotal
        {
            get { return grandtotal; }
            set
            {
                grandtotal = value;
                OnPropertyChanged("GrandTotal");
            }
        }

        public Store Store
        {
            get
            {
                return store;
            }

            set
            {
                store = value;
            }
        }
        #endregion

        private Store store;

        public PosManager()
        {
            store = new Store();

            SaleButtons = new ObservableCollection<PosButtonViewModel>();
            CurrentSaleList = new ObservableCollection<SaleItemViewModel>();                        

            CancelCommand = new PosButtonCommand(clearTransaction);
            PayCommand = new PosButtonCommand(finishTransaction);

            Total = "$0.00";
            Tax = "$0.00";
            GrandTotal = "$0.00";

            store.LoadSampleProducts();
            refreshProductButtons();
        }

        public void LoadProductsFromXml(string path)
        {
            try
            {
                store.LoadProductsFromXml(path);
                refreshProductButtons();
                clearTransaction(null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ConvertCompressedFile(string path)
        {
            var outPath = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + ".txt");

            try
            {
                Compressor.ConvertLog(path, outPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void addItem(object o)
        {
            var id = (int)o;

            store.AddItemToSale(id);
            refreshSale();
        }

        private void refreshSale()
        {
            CurrentSaleList.Clear();
            foreach (var s in store.GetSaleListing())
                CurrentSaleList.Add(new SaleItemViewModel(s));
            Total = store.SaleTotal.ToString("c2");
            Tax = store.SaleTax.ToString("c2");
            GrandTotal = store.GrandTotal.ToString("c2");
        }

        public void refreshProductButtons()
        {
            SaleButtons.Clear();
            foreach (var item in store.GetProductCatalog())
                SaleButtons.Add(new PosButtonViewModel(item.Name, new PosButtonCommand(addItem), item.Id));
        }

        private void clearTransaction(object o)
        {
            store.CancelSale();
            refreshSale();
        }

        private void finishTransaction(object o)
        {
            try
            {
                store.FinishSale();
                refreshSale();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void DisableSaver()
        {
            store.SetSaveSystem(SaveSystem.None);
        }

        public void UsePipeSaver()
        {
            store.SetSaveSystem(SaveSystem.Pipe);
        }

        public void UseTcpSaver()
        {
            store.SetSaveSystem(SaveSystem.Tcp);
        }
        
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
