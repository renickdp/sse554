﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PosSystem
{
    /// <summary>
    /// Interaction logic for PaymentWindow.xaml
    /// </summary>
    public partial class PaymentWindow : Window
    {
        public PaymentWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //Generate encryption key
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters RSAKeyInfo = RSA.ExportParameters(false);

            var cardType = "";
            if (MCBtn.IsChecked == true)
            {
                cardType = "Master Card";
            }
            else if (VisaBtn.IsChecked == true)
            {
                cardType = "Visa";
            }
            var cardName = CardNameTextBox.Text;
            var cardNumber = CardNumberTextBox.Text;
            var Month = MonthTextBox.Text;
            var Year = YearTextBox.Text;



            //encrypt
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] encryptedtext;
            var Cardenc = ByteConverter.GetBytes(cardNumber);
            encryptedtext = Asymmetric.RSAEncrypt(Cardenc, RSA.ExportParameters(false), false);
            EncKeyTextBox.Text = ByteConverter.GetString(encryptedtext);

        }


        private void DecryptBtn_Click(object sender, RoutedEventArgs e)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
            UnicodeEncoding ByteConverter = new UnicodeEncoding();
            byte[] encryptedtext;

            var Cardenc = ByteConverter.GetBytes(CardNumberTextBox.Text);
            encryptedtext =Asymmetric.RSAEncrypt(Cardenc, RSA.ExportParameters(false), false);

            byte[] decryptedtex = Asymmetric.RSADecrypt(encryptedtext, RSA.ExportParameters(true), false);
            DecryptTextBox.Text = ByteConverter.GetString(decryptedtex);



        }
    }
}
