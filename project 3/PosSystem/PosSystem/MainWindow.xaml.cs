﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PosSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void saleList_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            nameColumn.Width = 3 * saleList.ActualWidth / 5 - 2;
            valueColumn.Width = 2 * saleList.ActualWidth / 5 - 2;
        }

        private void grayThemeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Style buttonStyle = FindResource("GrayButtonTheme") as Style;
            Style gridStyle = FindResource("GrayBackgroundTheme") as Style;
            this.Resources["ButtonTheme"] = buttonStyle;
            this.Resources["BackgroundTheme"] = gridStyle;
        }

        private void blueThemeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Style buttonStyle = FindResource("BlueButtonTheme") as Style;
            Style gridStyle = FindResource("BlueBackgroundTheme") as Style;
            this.Resources["ButtonTheme"] = buttonStyle;
            this.Resources["BackgroundTheme"] = gridStyle;
        }

        private void CardPayButton_Click(object sender, RoutedEventArgs e)
        {
            var about = new PaymentWindow();
            about.Show();
        }

        private void Saver_Checked(object sender, RoutedEventArgs e)
        {
            var manager = DataContext as PosManager;

            if (TcpSaver.IsChecked)
                manager.UseTcpSaver();
            else if (PipeSaver.IsChecked)
                manager.UsePipeSaver();
            else
                manager.DisableSaver();
        }

        private void OpenXml_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            ofd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PosSystem\";
            if (ofd.ShowDialog() == true)
            {
                var manager = DataContext as PosManager;
                manager.LoadProductsFromXml(ofd.FileName);
            }
        }

        private void Log_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "log files (*.zip)|*.zip|All files (*.*)|*.*";
            ofd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\DataRecorderServer\";
            if (ofd.ShowDialog() == true)
            {
                var manager = DataContext as PosManager;
                manager.ConvertCompressedFile(ofd.FileName);
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void EditCatalog_Click(object sender, RoutedEventArgs e)
        {
            var manager = DataContext as PosManager;
            EditCatalogForm ecf = new EditCatalogForm(manager);
            ecf.Visible = true;
        }
    }
}
