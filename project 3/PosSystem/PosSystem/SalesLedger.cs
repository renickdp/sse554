﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataRecorderServer;

namespace PosSystem
{  
    public enum SaveSystem{
        None,
        Tcp,
        Pipe
    }

    public class SalesLedger
    {
        private List<Transaction> sales;
        public SaveSystem Saver;

        public SalesLedger()
        {
            sales = new List<Transaction>();
            Saver = SaveSystem.None;
        }

        public void Add(Transaction sale)
        {
            sales.Add(sale.Clone() as Transaction);
            Save(sale);
        }

        public void Save(Transaction sale)
        {
            switch (Saver)
            {
                case SaveSystem.Tcp:
                    SimpleClient sc = new SimpleClient(1997);
                    var data = Encoding.ASCII.GetBytes(sale.ToString());
                    sc.SendData(data);
                    break;

                case SaveSystem.Pipe:
                    var pm = new PipeManager("PosSystemPipe");
                    data = Encoding.ASCII.GetBytes(sale.ToString());
                    pm.SendData(data);
                    break;
            }
        }
    }
}
