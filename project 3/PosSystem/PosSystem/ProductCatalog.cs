﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace PosSystem
{
    public class ProductCatalog
    {
        private List<Product> catalog;
        public List<Product> Catalog
        {
            get { return catalog; }
        }

        public ProductCatalog()
        {
            catalog = new List<Product>();
        }
       
        public Product Lookup(int id)
        {
            return catalog.Where(p => p.Id == id).FirstOrDefault();
        }

        public void LoadSample()
        {
            catalog.Clear();
            catalog.Add(new Product(1, "Item 1", 100));
            catalog.Add(new Product(2, "Item 2", 50));
            catalog.Add(new Product(3, "Item 3", 250));
            catalog.Add(new Product(4, "Item 4", 300));
            catalog.Add(new Product(5, "Item 5", 30));
            catalog.Add(new Product(6, "Item 6", 40));
            catalog.Add(new Product(7, "Item 7", 60));
            catalog.Add(new Product(8, "Item 8", 3));
            catalog.Add(new Product(9, "Item 9", 10));
            catalog.Add(new Product(10, "Item 10", 1000));
        }

        public void LoadFromXml(string path)
        {
            if (File.Exists(path))
            {
                catalog.Clear();

                using (var s = File.OpenRead(path))
                {
                    var doc = new XmlDocument();
                    doc.Load(s);

                    XmlNodeList items = doc.GetElementsByTagName("item");
                    foreach(XmlNode item in items)
                    {
                        int id;
                        double price;
                        string name;

                        name = item["name"].InnerText;
                        Int32.TryParse(item["id"].InnerText, out id);
                        Double.TryParse(item["price"].InnerText, out price);

                        catalog.Add(new Product(id, name, price));
                    }
                }
            }
        }
    }
}
