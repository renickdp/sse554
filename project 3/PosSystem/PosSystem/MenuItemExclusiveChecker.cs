﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PosSystem
{
    public static class MenuItemExclusiveChecker
    {
        public static Dictionary<MenuItem, String> ElementsInCheckGroup = new Dictionary<MenuItem, string>();

        public static readonly DependencyProperty CheckGroupProperty = DependencyProperty.RegisterAttached("CheckGroup", typeof(string), typeof(MenuItemExclusiveChecker), new PropertyMetadata(String.Empty, OnGroupNameChanged));

        public static void SetCheckGroup(MenuItem element, string value)
        {
            element.SetValue(CheckGroupProperty, value);
        }

        public static String GetCheckGroup(MenuItem element)
        {
            return element.GetValue(CheckGroupProperty).ToString();
        }

        private static void OnGroupNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var mItem = d as MenuItem;

            if(mItem != null)
            {
                string newName = e.NewValue.ToString();
                string oldName = e.OldValue.ToString();
                if(String.IsNullOrEmpty(newName))
                {
                    RemoveCheckboxFromGroup(mItem);
                }
                else
                {
                    if(newName != oldName)
                    {
                        if(!String.IsNullOrEmpty(oldName))
                        {
                            RemoveCheckboxFromGroup(mItem);
                        }
                        ElementsInCheckGroup.Add(mItem, newName);
                        mItem.Checked += MenuItemChecked;
                    }
                }
            }
        }

        private static void RemoveCheckboxFromGroup(MenuItem checkbox)
        {
            ElementsInCheckGroup.Remove(checkbox);
            checkbox.Checked -= MenuItemChecked;
        }

        static void MenuItemChecked(object sender, RoutedEventArgs e)
        {
            var menuItem = e.OriginalSource as MenuItem;
            foreach (var item in ElementsInCheckGroup)
            {
                if (item.Key != menuItem && item.Value == GetCheckGroup(menuItem))
                {
                    item.Key.IsChecked = false;
                }
            }
        }
    }
}
