﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace CreditAndInterestServer
{
    public delegate void StatusReporter(string message);

    public class SimpleServer
    {
        private IPAddress address;
        private int port;
        private StatusReporter reporter;
        private CancellationTokenSource cancelToken;
        private InterestRateCalculator rateCalc;

        public double InterestRate;

        public SimpleServer()
        {
            address = IPAddress.Any;
            port = 1997;
            reporter = Console.WriteLine;
            cancelToken = new CancellationTokenSource();
            InterestRate = 0;
            rateCalc = new InterestRateCalculator(reporter);
        }

        public SimpleServer(int port)
        {
            address = IPAddress.Any;
            this.port = port;
            this.reporter = Console.WriteLine;
            cancelToken = new CancellationTokenSource();
            InterestRate = 0;
            rateCalc = new InterestRateCalculator(reporter);
        }

        public SimpleServer(int port, StatusReporter reporter)
        {
            address = IPAddress.Any;
            this.port = port;
            this.reporter = reporter;
            cancelToken = new CancellationTokenSource();
            InterestRate = 0;
            rateCalc = new InterestRateCalculator(reporter);
        }

        public void StopServer()
        {
            cancelToken.Cancel();
        }

        public Task RunServer()
        {
            return Task.Run(() =>
            {
                var listener = new TcpListener(address, port);

                try
                {
                    reporter(String.Format("Listening to requests on port {0}", port));
                    listener.Start();                    

                    while(!cancelToken.IsCancellationRequested)
                    {
                        IAsyncResult ar = listener.BeginAcceptTcpClient(RunClientRequest, listener);
                        ar.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(2), false);  
                    }
                }
                catch(Exception ex)
                {
                    reporter(ex.Message);
                }
                finally
                {
                    listener.Stop();
                }
            });
        }

        private void RunClientRequest(IAsyncResult ar)
        {
            try
            {
                TcpListener listener = (TcpListener)ar.AsyncState;

                if (listener.Server.IsBound)
                {
                    try
                    {
                        TcpClient client = listener.EndAcceptTcpClient(ar);
                        using (client)
                        {
                            reporter("Client has connected");
                            using (NetworkStream nStream = client.GetStream())
                            {
                                byte[] readBuffer = new byte[1024];
                                int read = nStream.Read(readBuffer, 0, readBuffer.Length);

                                string request = Encoding.ASCII.GetString(readBuffer, 0, read);
                                reporter(String.Format("Recieved {0}", request));

                                double rate = rateCalc.CalculateRate(request, InterestRate);

                                byte[] writeBuffer = BitConverter.GetBytes(rate);
                                nStream.Write(writeBuffer, 0, writeBuffer.Length);
                                reporter(String.Format("Sent {0}", rate));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        reporter(ex.Message);
                    }
                    reporter("Client disconnected");
                }
            }
            catch (Exception ex)
            {
                // Swallow "Object reference not set to an instance of an object." exception to prevent crash
            }            
        }
    }
}
