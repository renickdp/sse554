﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CreditAndInterestServer
{
    public class InterestRateCalculator
    {
        private Random random;
        private double creditSeed;
        private StatusReporter reporter;

        public InterestRateCalculator(StatusReporter reporter)
        {
            random = new Random();
            this.reporter = reporter;
        }

        public double CalculateRate(string name, double federalRate)
        {
            var taskCount = 3;

            var scores = GetCreditScores(taskCount, name);
            double scoreAvg = 0;
            double count = 0;
            foreach(var entry in scores)
            {
                for(int j = 0; j < entry.Value.Length; j++)
                {
                    scoreAvg = entry.Value[j];
                    count += 1;
                }
            }

            scoreAvg = scoreAvg / count;

            double rate = federalRate + (800 - scoreAvg) / 10000;

            return rate;
        }

        class CreditThreadData
        {
            public string Name;
            public int Loops;
            public int TaskNum;
            public double[] CreditScores;
            public Barrier Barrier;
        }

        private Dictionary<int, double[]> GetCreditScores(int taskCount, string name)
        {
            int loopCount = 3;

            var taskResults = new Dictionary<int, double[]>();

            Barrier barrier = new Barrier(1);
            
            creditSeed = random.NextDouble() * 100;

            for(int i = 0; i < taskCount; i++)
            {
                barrier.AddParticipant();

                taskResults.Add(i, new double[loopCount]);

                var ctd = new CreditThreadData() { Name = name, Loops = loopCount, TaskNum = i, CreditScores = taskResults[i], Barrier = barrier };

                Thread newThread = new Thread(CreditScoreFetcher);
                newThread.Start(ctd);
            }

            for(int i = 0; i < loopCount; i++)
            {
                reporter(String.Format("Waiting for credit score batch {0}", i));
                barrier.SignalAndWait();
                reporter(String.Format("Batch {0} Complete", i));
            }
            
            return taskResults;
        }

        private void CreditScoreFetcher(object data)
        {
            var ctd = data as CreditThreadData;

            Barrier barrier = ctd.Barrier;
            string name = ctd.Name;
            int loops = ctd.Loops;
            int taskNum = ctd.TaskNum;
            double[] creditScores = ctd.CreditScores;

            for (int i = 0; i < loops; i++)
            {
                var score = CreditScorer(name);

                creditScores[i] = score;
                barrier.SignalAndWait();
            }

            barrier.RemoveParticipant();
        }

        private double CreditScorer(string name)
        {
            double nameRate = name.GetHashCode() % 300;
            double value = 0;

            lock (random)
            {
                value = 300 + nameRate + random.NextDouble() * 200;
            }

            return value; 
        }
    }
}
