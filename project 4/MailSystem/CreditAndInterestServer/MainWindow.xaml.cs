﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CreditAndInterestServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer = new DispatcherTimer(); 

        public MainWindow()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(10);
            timer.Tick += OnTick;
            timer.Start();
        }

        private void OnTick(object sender, object e)
        {
            var dc = this.DataContext as ServerManager;
            if(dc != null)
            {
                dc.UpdateFedRate();
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = this.DataContext as ServerManager;
            if(dc != null)
            {
                dc.StartServer();
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = this.DataContext as ServerManager;
            if (dc != null)
            {
                try
                {
                    dc.StopServer();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ((TextBox)sender).ScrollToEnd();
        }
    }
}
