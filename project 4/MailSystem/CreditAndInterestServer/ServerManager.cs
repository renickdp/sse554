﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CreditAndInterestServer
{
    public class ServerManager : INotifyPropertyChanged
    {
        private DateTime lastCheck;
        public DateTime LastCheck
        {
            get { return lastCheck; }
            set
            {
                lastCheck = value;
                OnPropertyChanged("LastCheck");
            }
        }

        private double currentFedRate;
        public double CurrentFedRate
        {
            get { return currentFedRate; }
            set
            {
                currentFedRate = value;
                OnPropertyChanged("CurrentFedRate");
            }
        }

        public bool isRunning;
        public bool IsRunning
        {
            get { return isRunning; }
            set
            {
                isRunning = value;
                OnPropertyChanged("IsRunning");
            }
        }

        public bool isNotRunning;
        public bool IsNotRunning
        {
            get { return isNotRunning; }
            set
            {
                isNotRunning = value;
                OnPropertyChanged("IsNotRunning");
            }
        }

        private string activityLog;
        public string ActivityLog
        {
            get { return activityLog; }
            set
            {
                activityLog = value;
                OnPropertyChanged("ActivityLog");
            }
        }        

        private StringBuilder runningLog;
        private SimpleServer server;
        private Task serverRunner;

        private Random random;

        public ServerManager()
        {
            random = new Random();
            IsRunning = false;
            IsNotRunning = true;
            ActivityLog = "";
            runningLog = new StringBuilder();
            server = new SimpleServer(1997, WriteLog);

            UpdateFedRate();
        }

        public void UpdateFedRate()
        {
            LastCheck = DateTime.Now;
            CurrentFedRate = random.NextDouble() * (0.02) + 0.01;
            server.InterestRate = CurrentFedRate;
        }

        public void WriteLog(string message)
        {
            runningLog.AppendLine(message);
            ActivityLog = runningLog.ToString();
        }

        public void StartServer()
        {
            if(serverRunner == null)
            {
                serverRunner = server.RunServer();
                IsRunning = true;
                IsNotRunning = false;
            }            
        }

        public void StopServer()
        {
            if (serverRunner != null)
            {
                server.StopServer();
                Task.WaitAll(serverRunner);
                IsRunning = false;
                IsNotRunning = true;
                serverRunner = null;   
            }
        }

        #region INotifyPropertyChanged implmentation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
