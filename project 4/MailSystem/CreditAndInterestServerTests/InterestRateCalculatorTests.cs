﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditAndInterestServer;

namespace CreditAndInterestServerTests
{
    [TestClass]
    public class InterestRateCalculatorTests
    {
        [TestMethod]
        public void InterestRateCalculatorTests_CalculateRateTest()
        {
            var calc = new InterestRateCalculator(System.Console.WriteLine);

            var value = calc.CalculateRate("Test name", 1.0);

            Assert.IsTrue(value > 1.0 && value < 2.0);
        }
    }
}
