﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditAndInterestServer;

namespace CreditAndInterestServerTests
{
    [TestClass]
    public class ServerManagerTests
    {
        [TestMethod]
        public void ServerManagerTests_UpdateFedRateTest()
        {
            var sm = new ServerManager();

            sm.UpdateFedRate();
            var date = sm.LastCheck;
            var rate = sm.CurrentFedRate;

            Thread.Sleep(10);

            sm.UpdateFedRate();

            Assert.AreNotEqual(rate, sm.CurrentFedRate);
            Assert.AreNotEqual(date, sm.LastCheck);
        }

        [TestMethod]
        public void ServerManagerTests_WriteLogTest()
        {
            var sm = new ServerManager();

            sm.WriteLog("Test Log");
            sm.WriteLog("Second Record");

            Assert.IsTrue(sm.ActivityLog.Contains("Test Log"));
            Assert.IsTrue(sm.ActivityLog.Contains("Second Record"));
        }

        [TestMethod]
        public void ServerManagerTests_StartAndStopServerTest()
        {
            var sm = new ServerManager();

            sm.StartServer();

            Assert.AreEqual(true, sm.IsRunning);
            Assert.AreEqual(false, sm.IsNotRunning);

            sm.StopServer();

            Assert.AreEqual(false, sm.IsRunning);
            Assert.AreEqual(true, sm.IsNotRunning);
        }
    }
}
