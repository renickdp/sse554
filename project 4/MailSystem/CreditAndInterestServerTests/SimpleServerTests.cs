﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditAndInterestServer;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace CreditAndInterestServerTests
{
    [TestClass]
    public class SimpleServerTests
    {
        StringBuilder sb;

        void TestReporter(string message)
        {
            sb.Append(message);
        }

        [TestMethod]
        public void SimpleServerTests_StopAndRunServerTest()
        {
            int port = 2000;
            double rate = 0;
            sb = new StringBuilder();
            var ss = new SimpleServer(port, TestReporter);

            Task server = ss.RunServer();

            using (var client = new TcpClient())
            {
                client.Connect("localhost", port);

                using (NetworkStream nStream = client.GetStream())
                {
                    byte[] buffer = Encoding.ASCII.GetBytes("Test Message");

                    nStream.Write(buffer, 0, buffer.Length);
                    nStream.Flush();

                    byte[] readBuffer = new byte[1024];
                    int read = nStream.Read(readBuffer, 0, readBuffer.Length);

                    rate = BitConverter.ToDouble(readBuffer, 0);
                }
            }

            Assert.IsTrue(sb.ToString().Contains("Test Message"));
            Assert.AreNotEqual(0, rate);

            ss.StopServer();

            Task.WaitAll(server);
        }
    }
}
