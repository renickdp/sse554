﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSystem;
using System.Threading;
using System.IO;

namespace UnitTestProject1
{
    [TestClass]
    public class LoanTests
    {
        [TestMethod]
        public void LoanTests_ConstructorTest1()
        {
            var loan1 = new Loan("Test Name", "Test Address");

            loan1.City = "Test City";
            loan1.State = "Test State";
            loan1.Zip = 3000;
            loan1.Email = "Test Email";
            loan1.InitialAmount = 100;
            loan1.CurrentAmount = 50;
            loan1.Period = 30;
            loan1.Payment = 20;
            loan1.Rate = 0.05;

            var loan2 = new Loan(loan1);

            loan1.City = "Test City 2";

            Assert.AreEqual(loan1.Name, loan2.Name);
            Assert.AreEqual(loan1.Address, loan2.Address);
            Assert.AreNotEqual(loan1.City, loan2.City);
            Assert.AreEqual(loan1.Zip, loan2.Zip);
            Assert.AreEqual(loan1.Email, loan2.Email);
            Assert.AreEqual(loan1.InitialAmount, loan2.InitialAmount);
            Assert.AreEqual(loan1.CurrentAmount, loan2.CurrentAmount);
            Assert.AreEqual(loan1.Period, loan2.Period);
            Assert.AreEqual(loan1.Payment, loan2.Payment);
            Assert.AreEqual(loan1.Rate, loan2.Rate);
        }

        [TestMethod]
        public void LoanTests_CopyValuesTest()
        {
            var loan1 = new Loan("Test Name", "Test Address");

            loan1.City = "Test City";
            loan1.State = "Test State";
            loan1.Zip = 3000;
            loan1.Email = "Test Email";
            loan1.InitialAmount = 100;
            loan1.CurrentAmount = 50;
            loan1.Period = 30;
            loan1.Payment = 20;
            loan1.Rate = 0.05;

            var loan2 = new Loan();            

            Assert.AreNotEqual(loan1.Name, loan2.Name);
            Assert.AreNotEqual(loan1.Address, loan2.Address);
            Assert.AreNotEqual(loan1.City, loan2.City);
            Assert.AreNotEqual(loan1.Zip, loan2.Zip);
            Assert.AreNotEqual(loan1.Email, loan2.Email);
            Assert.AreNotEqual(loan1.InitialAmount, loan2.InitialAmount);
            Assert.AreNotEqual(loan1.CurrentAmount, loan2.CurrentAmount);
            Assert.AreNotEqual(loan1.Period, loan2.Period);
            Assert.AreNotEqual(loan1.Payment, loan2.Payment);
            Assert.AreNotEqual(loan1.Rate, loan2.Rate);

            loan2.CopyValues(loan1);

            Assert.AreEqual(loan1.Name, loan2.Name);
            Assert.AreEqual(loan1.Address, loan2.Address);
            Assert.AreEqual(loan1.City, loan2.City);
            Assert.AreEqual(loan1.Zip, loan2.Zip);
            Assert.AreEqual(loan1.Email, loan2.Email);
            Assert.AreEqual(loan1.InitialAmount, loan2.InitialAmount);
            Assert.AreEqual(loan1.CurrentAmount, loan2.CurrentAmount);
            Assert.AreEqual(loan1.Period, loan2.Period);
            Assert.AreEqual(loan1.Payment, loan2.Payment);
            Assert.AreEqual(loan1.Rate, loan2.Rate);
        }

        [TestMethod]
        public void LoanTests_SaveAndLoadTest()
        {
            var folder = @"C:\MailSystem Test Folder";
            var file = @"C:\MailSystem Test Folder\testloan.xml";

            var loan1 = new Loan("Test Name", "Test Address");
            loan1.City = "Test City";
            loan1.State = "Test State";
            loan1.Zip = 3000;
            loan1.Email = "Test Email";
            loan1.InitialAmount = 100;
            loan1.CurrentAmount = 50;
            loan1.Period = 30;
            loan1.Payment = 20;
            loan1.Rate = 0.05;

            var di = new DirectoryInfo(folder);
            if (di.Exists)
                Directory.Delete(folder, true);

            Helper.CheckFolder(folder);

            Thread.Sleep(10);

            loan1.SaveToFile(file);

            var loan2 = Loan.LoadFromFile(file);

            Assert.AreEqual(loan1.Name, loan2.Name);
            Assert.AreEqual(loan1.Address, loan2.Address);
            Assert.AreEqual(loan1.City, loan2.City);
            Assert.AreEqual(loan1.Zip, loan2.Zip);
            Assert.AreEqual(loan1.Email, loan2.Email);
            Assert.AreEqual(loan1.InitialAmount, loan2.InitialAmount);
            Assert.AreEqual(loan1.CurrentAmount, loan2.CurrentAmount);
            Assert.AreEqual(loan1.Period, loan2.Period);
            Assert.AreEqual(loan1.Payment, loan2.Payment);
            Assert.AreEqual(loan1.Rate, loan2.Rate);
        }
    }
}
