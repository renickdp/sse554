﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSystem;
using System.IO;
using System.Threading;

namespace UnitTestProject1
{
    /// <summary>
    /// Summary description for HelperTests
    /// </summary>
    [TestClass]
    public class HelperTests
    {
        [TestMethod]
        public void HelperTests_CheckFolderTest()
        {
            var logFolder = @"C:\MailSystem Test Folder";
            var di = new DirectoryInfo(logFolder);
            if (di.Exists)
                Directory.Delete(logFolder, true);

            Thread.Sleep(10);

            Helper.CheckFolder(logFolder);

            Thread.Sleep(10);

            di = new DirectoryInfo(logFolder);
            Assert.IsTrue(di.Exists);
        }
    }
}
