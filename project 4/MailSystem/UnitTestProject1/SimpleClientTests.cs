﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSystem;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Text;
using System.Net;

namespace UnitTestProject1
{
    [TestClass]
    public class SimpleClientTests
    {
        double expected = 1000;
        string recievedString;

        Task RunServerHelper()
        {
            return Task.Factory.StartNew(() =>
            {
                var listener = new TcpListener(IPAddress.Any, 2000);

                listener.Start();

                using (var client = listener.AcceptTcpClient())
                using (NetworkStream stream = client.GetStream())
                {
                    byte[] readBuffer = new byte[1024];
                    int read = stream.Read(readBuffer, 0, readBuffer.Length);

                    recievedString = Encoding.ASCII.GetString(readBuffer, 0, read);

                    byte[] writeBuffer = BitConverter.GetBytes(expected);
                    stream.Write(writeBuffer, 0, writeBuffer.Length);
                }
            });
        }

        [TestMethod]
        public void SimpleClientTests_GetRateTest()
        {
            int port = 2000;
            var sc = new SimpleClient(port);

            Task server = RunServerHelper();
            Task<double> client = sc.GetRate("test");

            Task.WaitAll(new Task[] { client, server });

            double actual = client.Result;
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(recievedString, "test");
        }
    }
}
