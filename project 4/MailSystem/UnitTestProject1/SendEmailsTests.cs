﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSystem;

namespace UnitTestProject1
{
    [TestClass]
    public class SendEmailsTest
    {

        [TestMethod]
        public void moreThreadsThanLoans()
        {
            SystemManager manager = new SystemManager();
            manager.ThreadCount = 4;
            manager.Company = new LendingCompany();
            manager.Company.CreateLoan("loan1", "address1");
            manager.Company.CreateLoan("loan2", "address2");
            manager.UpdateLoanList();
            manager.SendEmails();
            System.Threading.Thread.Sleep(3000);
            Assert.AreEqual(2,manager.EmailCount);
        }

        [TestMethod]
        public void oddThreadsAndEvenLoans()
        {
            SystemManager manager = new SystemManager();
            manager.ThreadCount = 3;
            manager.Company = new LendingCompany();
            manager.Company.CreateLoan("loan1", "address1");
            manager.Company.CreateLoan("loan2", "address2");
            manager.Company.CreateLoan("loan3", "address3");
            manager.Company.CreateLoan("loan4", "address4");
            manager.Company.CreateLoan("loan5", "address5");
            manager.Company.CreateLoan("loan6", "address6");
            manager.Company.CreateLoan("loan7", "address7");
            manager.Company.CreateLoan("loan8", "address8");
            manager.UpdateLoanList();
            manager.SendEmails();
            System.Threading.Thread.Sleep(9000);
            Assert.AreEqual(8,manager.EmailCount);
        }

        [TestMethod]
        public void evenThreadsAndOddLoans()
        {
            SystemManager manager = new SystemManager();
            manager.ThreadCount = 4;
            manager.Company = new LendingCompany();
            manager.Company.CreateLoan("loan1", "address1");
            manager.Company.CreateLoan("loan2", "address2");
            manager.Company.CreateLoan("loan3", "address3");
            manager.Company.CreateLoan("loan4", "address4");
            manager.Company.CreateLoan("loan5", "address5");
            manager.Company.CreateLoan("loan6", "address6");
            manager.Company.CreateLoan("loan7", "address7");
            manager.UpdateLoanList();
            manager.SendEmails();
            System.Threading.Thread.Sleep(8000);
            Assert.AreEqual(7,manager.EmailCount);
        }
    }
}
