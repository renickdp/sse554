﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MailSystem;
using System.IO;

namespace UnitTestProject1
{
    [TestClass]
    public class LendingCompanyTests
    {
        [TestMethod]
        public void LendingCompanyTests_CreateLoanTest1()
        {
            var company = new LendingCompany();
            
            company.CreateLoan("Test Name", "Test Address");

            Assert.AreEqual("Test Name", company.Loans[0].Name);
            Assert.AreEqual("Test Address", company.Loans[0].Address);
        }

        [TestMethod]
        public void LendingCompanyTests_CreateLoanTest2()
        {
            var company = new LendingCompany();

            var loan1 = new Loan();
            loan1.Name = "Test Name";
            loan1.Address = "Test Address";
            loan1.City = "Test City";
            loan1.State = "Test State";
            loan1.Zip = 3000;
            loan1.Email = "Test Email";
            loan1.InitialAmount = 100;
            loan1.CurrentAmount = 50;
            loan1.Period = 30;
            loan1.Payment = 20;
            loan1.Rate = 0.05;

            company.CreateLoan(loan1);

            Assert.AreNotEqual(loan1, company.Loans[0]);
            Assert.AreEqual(loan1.Name, company.Loans[0].Name);
            Assert.AreEqual(loan1.Address, company.Loans[0].Address);
            Assert.AreEqual(loan1.City, company.Loans[0].City);
            Assert.AreEqual(loan1.Zip, company.Loans[0].Zip);
            Assert.AreEqual(loan1.Email, company.Loans[0].Email);
            Assert.AreEqual(loan1.InitialAmount, company.Loans[0].InitialAmount);
            Assert.AreEqual(loan1.CurrentAmount, company.Loans[0].CurrentAmount);
            Assert.AreEqual(loan1.Period, company.Loans[0].Period);
            Assert.AreEqual(loan1.Payment, company.Loans[0].Payment);
            Assert.AreEqual(loan1.Rate, company.Loans[0].Rate);
        }

        [TestMethod]
        public void LendingCompanyTests_ExportImportTest()
        {
            var folder = @"C:\MailSystem Test Folder";
            var company1 = new LendingCompany();

            var loan1 = new Loan();
            loan1.Name = "Test Name";
            loan1.Address = "Test Address";
            loan1.City = "Test City";
            loan1.State = "Test State";
            loan1.Zip = 3000;
            loan1.Email = "Test Email";
            loan1.InitialAmount = 100;
            loan1.CurrentAmount = 50;
            loan1.Period = 30;
            loan1.Payment = 20;
            loan1.Rate = 0.05;

            company1.CreateLoan(loan1);
            company1.CreateLoan("Test Name2", "Test Address2");

            var di = new DirectoryInfo(folder);
            if (di.Exists)
                Directory.Delete(folder, true);

            Helper.CheckFolder(folder);

            Thread.Sleep(10);

            company1.ExportLoans(folder);

            var company2 = new LendingCompany();

            company2.ImportLoans(folder);

            for(int i = 0; i < company2.Loans.Count; i++)
            {
                Assert.AreEqual(company1.Loans[i].Name, company2.Loans[i].Name);
                Assert.AreEqual(company1.Loans[i].Address, company2.Loans[i].Address);
                Assert.AreEqual(company1.Loans[i].City, company2.Loans[i].City);
                Assert.AreEqual(company1.Loans[i].Zip, company2.Loans[i].Zip);
                Assert.AreEqual(company1.Loans[i].Email, company2.Loans[i].Email);
                Assert.AreEqual(company1.Loans[i].InitialAmount, company2.Loans[i].InitialAmount);
                Assert.AreEqual(company1.Loans[i].CurrentAmount, company2.Loans[i].CurrentAmount);
                Assert.AreEqual(company1.Loans[i].Period, company2.Loans[i].Period);
                Assert.AreEqual(company1.Loans[i].Payment, company2.Loans[i].Payment);
                Assert.AreEqual(company1.Loans[i].Rate, company2.Loans[i].Rate);
            }
        }
    }
}
