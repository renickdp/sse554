﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using Microsoft.Office;

namespace MailSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ImportLoans_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if(dc != null)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                Helper.CheckFolder(dc.WorkingFolder);
                fbd.SelectedPath = dc.WorkingFolder;
                var result = fbd.ShowDialog();
                if(result == System.Windows.Forms.DialogResult.OK)
                {
                    dc.ImportLoans(fbd.SelectedPath);
                    loadEditComboBox.SelectedIndex = 0;
                }
            } 
        }

        private void ExportLoans_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if (dc != null)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                Helper.CheckFolder(dc.WorkingFolder);
                fbd.SelectedPath = dc.WorkingFolder;
                var result = fbd.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    dc.ExportLoans(fbd.SelectedPath);
                    loadEditComboBox.SelectedIndex = 0;
                }
            } 
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void mailList_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            addressColumn.Width = mailList.ActualWidth / 2 - 2;
            paymentColumn.Width = mailList.ActualWidth / 2 - 2;
        }

        private void sendEmailButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if (dc != null)
            {
                dc.SendEmails();
            }
        }

        private async void updatePaymentsButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if (dc != null)
            {
                List<Task> tasks = dc.UpdatePayments(this.Dispatcher);
                foreach (Task task in tasks)
                    await task;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            var cb = sender as System.Windows.Controls.ComboBox;
            if(dc != null && cb != null)
            {
               dc.SelectLoan(cb.SelectedIndex);
            }
        }

        private void newLoanButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if(dc != null)
            {
                if (dc.CreateNewLoan())
                    loadEditComboBox.SelectedIndex = dc.LoanList.Count - 1;
                System.Windows.MessageBox.Show("Loan added");
            }
        }

        private void updateLoanButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            var i = loadEditComboBox.SelectedIndex;
            if (dc != null)
            {
                if (dc.ReplaceLoan(i))
                    loadEditComboBox.SelectedIndex = i;
                System.Windows.MessageBox.Show("Loan updated");
            }
        }

        private async void interestRateButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if(dc != null)
            {
                try
                {
                    await dc.GetInterestRate(dc.EditingLoan);
                }
                catch(Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }

        private void ExportCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as SystemManager;
            if (dc != null)
            {
                foreach (var item in dc.LoanList)
                {
                    Object oMissing = System.Reflection.Missing.Value;
                    Object oTemplatePath = "C:\\Template.docx";
                    Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
                    Microsoft.Office.Interop.Word.Document wordDoc = new Microsoft.Office.Interop.Word.Document();
                    wordDoc = wordApp.Documents.Add(ref oTemplatePath, ref oMissing, ref oMissing, ref oMissing);

                    //address
                    wordDoc.Bookmarks["aname"].Select();
                    wordApp.Selection.TypeText(item.Name);
                    wordDoc.Bookmarks["address"].Select();
                    wordApp.Selection.TypeText(item.Address);
                    wordDoc.Bookmarks["city"].Select();
                    wordApp.Selection.TypeText(item.City);
                    wordDoc.Bookmarks["state"].Select();
                    wordApp.Selection.TypeText(item.State);
                    wordDoc.Bookmarks["zip"].Select();
                    wordApp.Selection.TypeText(Convert.ToString(item.Zip));
                    //Letter body
                    wordDoc.Bookmarks["name"].Select();
                    wordApp.Selection.TypeText(item.Name);
                    wordDoc.Bookmarks["payment"].Select();
                    wordApp.Selection.TypeText(Convert.ToString(item.Payment));
                    //loan details table
                    wordDoc.Bookmarks["tamount"].Select();
                    wordApp.Selection.TypeText(Convert.ToString(item.InitialAmount));
                    wordDoc.Bookmarks["tperiod"].Select();
                    wordApp.Selection.TypeText(Convert.ToString(item.Period));
                    wordDoc.Bookmarks["tinterest"].Select();
                    wordApp.Selection.TypeText(Convert.ToString(item.Rate));
                    wordDoc.Bookmarks["tpayment"].Select();
                    wordApp.Selection.TypeText(Convert.ToString(item.CurrentAmount));

                    wordDoc.SaveAs(Convert.ToString(item.Name) + "loan.doc");
                    wordApp.Documents.Open(Convert.ToString(item.Name) + "loan.doc");
                    wordApp.Application.Quit();

                }


            }
        }
    }
}
