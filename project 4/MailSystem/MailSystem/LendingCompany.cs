﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MailSystem
{
    public class LendingCompany
    {
        private const string LOAN_NAME_FMT = "Loan_Num_{0}.xml";

        private List<Loan> loans;
        public List<Loan> Loans
        {
            set { loans = value; }
            get { return loans; }
        }

        public LendingCompany()
        {
            loans = new List<Loan>();
        }

        public void CreateLoan(string name, string address)
        {
            loans.Add(new Loan(name, address));
        }

        public void CreateLoan(Loan newLoan)
        {
            loans.Add(new Loan(newLoan));
        }

        public void ExportLoans(string path)
        {
            Parallel.For(0, Loans.Count, i =>
            {
                string fileName = Path.Combine(path, String.Format(LOAN_NAME_FMT, i));
                Loans[i].SaveToFile(fileName);
            });
        }

        private int nextLoanToLoad;
        Mutex mutex;

        private void LoadLoan(string path)
        {
            string fileName;
            while(true)
            {
                if (mutex.WaitOne())
                {
                    try
                    {
                        fileName = Path.Combine(path, String.Format(LOAN_NAME_FMT, nextLoanToLoad));
                        nextLoanToLoad++;
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }
                else
                {
                    return;
                }

                if (File.Exists(fileName))
                {
                    var newLoan = Loan.LoadFromFile(fileName);

                    lock (loans)
                    {
                        loans.Add(newLoan);
                    }
                }
                else
                {
                    return;
                }
            }
        }

        public void ImportLoans(string path)
        {
            int taskCount = 3;
            var tasks = new Task[taskCount];
            nextLoanToLoad = 0;

            if (mutex == null)
                mutex = new Mutex(false, "LoadLoadingMutex");

            loans.Clear();
            for(int i = 0; i < taskCount; i++)
            {
                tasks[i] = Task.Run(() => LoadLoan(path));
            }

            Task.WaitAll(tasks);
        }
    }
}
