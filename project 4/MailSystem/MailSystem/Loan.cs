﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;

namespace MailSystem
{
    public class Loan : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private string address;
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged("Address");
            }
        }

        private string city;
        public string City
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("City");
            }
        }

        private string state;
        public string State
        {
            get { return state; }
            set
            {
                state = value;
                OnPropertyChanged("State");
            }
        }

        private int zip;
        public int Zip
        {
            get { return zip; }
            set
            {
                zip = value;
                OnPropertyChanged("Zip");
            }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        private double initialAmount;
        public double InitialAmount
        {
            get { return initialAmount; }
            set
            {
                initialAmount = value;
                OnPropertyChanged("InitialAmount");
            }
        }

        private double currentAmount;
        public double CurrentAmount
        {
            get { return currentAmount; }
            set
            {
                currentAmount = value;
                OnPropertyChanged("CurrentAmount");
            }
        }

        private double period;
        public double Period
        {
            get { return period; }
            set
            {
                period = value;
                OnPropertyChanged("Period");
            }
        }

        private double payment;
        public double Payment
        {
            get { return payment; }
            set
            {
                payment = value;
                OnPropertyChanged("Payment");
            }
        }

        private double rate;
        public double Rate
        {
            get { return rate; }
            set
            {
                rate = value;
                OnPropertyChanged("Rate");
            }
        }
        
        public Loan()
        {
            name = "";
            address = "";
            city = "";
            state = "";
            zip = 0;
            email = "";
            initialAmount = 0;
            currentAmount = 0;
            period = 0;
            payment = 0;
            rate = 0;
        }

        public Loan(Loan loan)
        {
            this.name = loan.name;
            this.address = loan.address;
            this.city = loan.city;
            this.state = loan.state;
            this.zip = loan.zip;
            this.email = loan.email;
            this.initialAmount = loan.initialAmount;
            this.currentAmount = loan.currentAmount;
            this.period = loan.period;
            this.payment = loan.payment;
            this.rate = loan.rate;
        }

        public Loan(string name, string address)
        {
            this.name = name;
            this.address = address;
            city = "";
            state = "";
            zip = 0;
            email = "";
            initialAmount = 0;
            currentAmount = 0;
            period = 0;
            payment = 0;
            rate = 0;
        }

        public void CopyValues(Loan loan)
        {
            this.Name = loan.name;
            this.Address = loan.address;
            this.City = loan.city;
            this.State = loan.state;
            this.Zip = loan.zip;
            this.Email = loan.email;
            this.InitialAmount = loan.initialAmount;
            this.CurrentAmount = loan.currentAmount;
            this.Period = loan.period;
            this.Payment = loan.payment;
            this.Rate = loan.rate;
        }

        public void SaveToFile(string file)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Loan));

            using(StreamWriter sw = new StreamWriter(file))
            {
                xs.Serialize(sw, this);
            }
        }

        public static Loan LoadFromFile(string file)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Loan));

            using (StreamReader sr = new StreamReader(file))
            {
                return (Loan)xs.Deserialize(sr);
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        #region INotifyPropertyChanged implmentation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
