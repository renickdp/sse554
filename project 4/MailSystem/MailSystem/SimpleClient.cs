﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace MailSystem
{
    public class SimpleClient
    {
        private string address;
        private int port;
        
        public SimpleClient()
        {
            this.address = "localhost";
            port = 1997;
        }

        public SimpleClient(int port)
        {
            this.address = "localhost";
            this.port = port;
        }

        public async Task<double> GetRate(string name)
        {
            double rate = 0;

            try
            {
                using (var client = new TcpClient())
                {
                    await client.ConnectAsync(address, port);

                    using (NetworkStream nStream = client.GetStream())
                    {
                        byte[] buffer = Encoding.ASCII.GetBytes(name);

                        await nStream.WriteAsync(buffer, 0, buffer.Length);
                        await nStream.FlushAsync();

                        byte[] readBuffer = new byte[1024];
                        int read = await nStream.ReadAsync(readBuffer, 0, readBuffer.Length);

                        rate = BitConverter.ToDouble(readBuffer, 0);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return rate;
        }
    }
}
