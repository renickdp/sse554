﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MailSystem
{
    public class SystemManager : INotifyPropertyChanged
    {
        private int emailCount = 0;
        public int EmailCount
        {
            get
            {
                return emailCount;
            }

            set
            {
                emailCount = value;
            }
        }
        private int threadCount = 3;
        public int ThreadCount
        {
            get
            {
                return threadCount;
            }

            set
            {
                threadCount = value;
            }
        }

        private ObservableCollection<Loan> loanList;
        public ObservableCollection<Loan> LoanList
        {
            get { return loanList; }
            set
            {
                loanList = value;
                OnPropertyChanged("LoanList");
            }
        }

        private Loan editingLoan;
        public Loan EditingLoan
        {
            get { return editingLoan; }
            set
            {
                editingLoan = value;
                OnPropertyChanged("EditingLoan");
                OnPropertyChanged("LoanList");
            }
        }

        private Loan blankLoan;
        private LendingCompany company;
        public LendingCompany Company
        {
            get
            {
                return company;
            }

            set
            {
                company = value;
            }
        }

        private SimpleClient interestClient;
        public string WorkingFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + @"\MailSystem\";

        public SystemManager()
        {
            LoanList = new ObservableCollection<Loan>();
            blankLoan = new Loan();

            company = new LendingCompany();
            interestClient = new SimpleClient();

            demo(); // Remove TBD
        }

        private void demo()
        {
            company.CreateLoan("Peter", "address1");
            company.CreateLoan("Joan", "address2");
            company.CreateLoan("Nick", "address3");
            company.CreateLoan("loan4", "address4");
            company.CreateLoan("loan5", "address5");
            company.CreateLoan("loan6", "address6");
            company.CreateLoan("loan7", "address7");
            company.CreateLoan("loan8", "address8");

            //company.CalculateLoanPayments();
            UpdateLoanList();
        }

        public bool CreateNewLoan()
        {
            company.CreateLoan(editingLoan);
            UpdateLoanList();
            SelectLoan(LoanList.Count - 1);
            return true;
        }

        public void SelectLoan(int i)
        {
            if (i >= 0 && i < LoanList.Count)
                EditingLoan.CopyValues(LoanList[i]);
        }

        public bool ReplaceLoan(int i)
        {
            if (i >= 0 && i < LoanList.Count)
            {
                LoanList[i].CopyValues(EditingLoan);
                return true;
            }

            return false;
        }

        private void ClearLoanList()
        {
            LoanList.Clear();
            editingLoan = blankLoan;
        }

        public void UpdateLoanList()
        {
            loanList.Clear();
            editingLoan = blankLoan;

            foreach (var loan in company.Loans)
                loanList.Add(loan);
            OnPropertyChanged("LoanList");

            SelectLoan(0);
        }

        public void ExportLoans(string folder)
        {
            ClearLoanList();
            company.ExportLoans(folder);
            UpdateLoanList();
        }

        public void ImportLoans(string folder)
        {
            ClearLoanList();
            company.ImportLoans(folder);
            UpdateLoanList();
        }

        public async Task GetInterestRate(Loan loan)
        {
            double newRate = await interestClient.GetRate(loan.Name);
            loan.Rate = newRate;
        }

        public void SendEmails()
        {
            //split the work equally into threads based on the value of threadCount
            //then traverse sublists of loanList based on the value of i
            Task.Factory.StartNew(() => Parallel.For(0, threadCount, i =>
            {
                int splitSize = loanList.Count / threadCount;

                //special case at the end of the loop to catch remainder of items
                if (i == threadCount - 1)
                {
                    foreach (Loan loan in loanList.ToList<Loan>().GetRange(i * splitSize, loanList.Count - (i * splitSize)))
                    {
                        System.Threading.Thread.Sleep(1000);
                        emailCount++;
                        System.Console.Write("\nEmail sent for the address: " + loan.Address);
                    }
                }
                else {
                    foreach (Loan loan in loanList.ToList<Loan>().GetRange(i * splitSize, splitSize))
                    {
                        System.Threading.Thread.Sleep(1000);
                        emailCount++;
                        System.Console.Write("\nEmail sent for the address: " + loan.Address);
                    }
                }
            }));
        }

        public void ExportCusromers()
        {

           
        }

        private class UpdateLoanPaymentData
        {
            public Dispatcher Dispatcher;
            public Loan Loan;
            public int LoanIndex;
            public Random random;
        }

        public List<Task> UpdatePayments(Dispatcher dispatcher)
        {
            List<Task> paymentTasks = new List<Task>();
            Random r = new Random();

            for (int i = 0; i < LoanList.Count; i++)
            {
                UpdateLoanPaymentData data = new UpdateLoanPaymentData() { Dispatcher = dispatcher, Loan = LoanList[i], LoanIndex = i, random = r };
                Task updateTask = new Task(UpdateLoanPayment, data, TaskCreationOptions.LongRunning);
                paymentTasks.Add(updateTask);
                updateTask.Start();
            }

            return paymentTasks;
        }

        private void UpdateLoanPayment(object o)
        {
            UpdateLoanPaymentData data = o as UpdateLoanPaymentData;
            int i = data.LoanIndex;
            double numerator = data.Loan.Rate * data.Loan.InitialAmount;
            double denominator = 1 - Math.Pow(1 + data.Loan.Rate, -1 * data.Loan.Period);

            var payment = numerator / denominator;
            int waitTime;

            Monitor.Enter(data.random);
            try
            {
                waitTime = data.random.Next(0, 5000);
            }
            finally
            {
                Monitor.Exit(data.random);
            }

            Thread.Sleep(waitTime);

            data.Dispatcher.Invoke((Action)(() =>
            {
                LoanList[i].Payment = payment;
            }));
        }

        #region INotifyPropertyChanged implmentation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
