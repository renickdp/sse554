﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountSystem
{
    public class Paycheck
    {
        private double amount;
        private DateTime date;

        public Paycheck()
        {
            amount = 0;
            this.date = DateTime.Now;
        }

        public Paycheck(double amount)
        {
            this.amount = amount;
            this.date = DateTime.Now;
        }

        public Paycheck(double amount, DateTime date)
        {
            this.amount = amount;
            this.date = date;
        }

        public double Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }
    }
}
