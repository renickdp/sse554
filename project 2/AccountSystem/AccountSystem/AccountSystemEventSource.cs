﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountSystem
{
    class AccountSystemEventSource : EventSource
    {
        private AccountSystemEventSource() : base()
        {

        }

        public static AccountSystemEventSource Log = new AccountSystemEventSource();

        public void ProgramStart()
        {
            base.WriteEvent(1);
        }

        public void UiInitialized()
        {
            base.WriteEvent(2);
        }

        public void FileWatcherStart()
        {
            base.WriteEvent(3);
        }

        public void FileWatcherStopped()
        {
            base.WriteEvent(4);
        }

        public void EmployeeCreated(string name)
        {
            base.WriteEvent(5, name + " created");
        }
    }
}
