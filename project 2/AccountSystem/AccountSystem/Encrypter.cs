﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AccountSystem
{
    public class Encrypter
    {
        private RijndaelManaged RmCrypto;
        private byte[] iv;
        private byte[] key;

        public Encrypter()
        {
            RmCrypto = new RijndaelManaged();

            this.key = new byte[16];
            this.iv = new byte[16];

            for(int i = 0; i < key.Length; i++)
            {
                this.key[i] = (byte)i;
                this.iv[i] = (byte)i;
            }

            RmCrypto.Padding = PaddingMode.PKCS7;      
        }
        
        public CryptoStream GetEncrypter(Stream stream)
        {
            return new CryptoStream(stream, RmCrypto.CreateEncryptor(key, iv), CryptoStreamMode.Write);
        }

        public CryptoStream GetDecrypter(Stream stream)
        { 
            return new CryptoStream(stream, RmCrypto.CreateDecryptor(key, iv), CryptoStreamMode.Read);
        }
    }
}
