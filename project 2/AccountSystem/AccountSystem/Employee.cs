﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountSystem
{
    public class Employee
    {
        private string name;
        private int age;
        private double salary;
        private DateTime joinDate;
        private List<Paycheck> paychecks;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }

        public DateTime JoinDate
        {
            get
            {
                return joinDate;
            }

            set
            {
                joinDate = value;
            }
        }

        public List<Paycheck> Paychecks
        {
            get
            {
                return paychecks;
            }

            set
            {
                paychecks = value;
            }
        }

        public double Salary
        {
            get
            {
                return salary;
            }

            set
            {
                salary = value;
            }
        }

        public Employee()
        {
            name = "";
            age = 0;
            salary = 0.0;
            joinDate = DateTime.Now;
            paychecks = new List<Paycheck>();
            AccountSystemEventSource.Log.EmployeeCreated("");
        }

        public Employee(string name)
        {
            this.name = name;
            age = 0;
            salary = 0.0;
            joinDate = DateTime.Now;
            paychecks = new List<Paycheck>();
            AccountSystemEventSource.Log.EmployeeCreated(name);
        }

        public Employee(string name, int age)
        {
            this.name = name;
            this.age = age;
            salary = 0.0;
            joinDate = DateTime.Now;
            paychecks = new List<Paycheck>();
            AccountSystemEventSource.Log.EmployeeCreated(name);
        }

        public Employee(string name, int age, double salary)
        {
            this.name = name;
            this.age = age;
            this.salary = salary;
            joinDate = DateTime.Now;
            paychecks = new List<Paycheck>();
            AccountSystemEventSource.Log.EmployeeCreated(name);
        }

        public Employee(string name, int age, DateTime joinDate)
        {
            this.name = name;
            this.age = age;
            salary = 0.0;
            this.joinDate = joinDate;
            paychecks = new List<Paycheck>();
            AccountSystemEventSource.Log.EmployeeCreated(name);
        }

        public Employee(string name, int age, double salary, DateTime joinDate)
        {
            this.name = name;
            this.age = age;
            this.salary = salary;
            this.joinDate = joinDate;
            paychecks = new List<Paycheck>();
            AccountSystemEventSource.Log.EmployeeCreated(name);
        }

        public void Pay(double amount, DateTime date)
        {
            paychecks.Add(new Paycheck(amount, date));
        }
    }
}
