﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace AccountSystem
{
    public class DatabaseWatcher
    {
        private FileSystemWatcher watcher;
        public bool UnitTesting;
        public bool FileChanged;
        public bool FileDeleted;

        public DatabaseWatcher(string path)
        {
            var test = new DirectoryInfo(path);
            UnitTesting = false;
            FileChanged = false;
            FileDeleted = false;
            if (test.Exists)
            {
                watcher = new FileSystemWatcher(path, "*.*");
            }
        }

        public void BeginWatching()
        {
            if(watcher != null)
            {
                watcher.EnableRaisingEvents = true;
                watcher.Changed += OnFileChanged;
                watcher.Deleted += OnFileDeleted;
            }
        }

        public void StopWatching()
        {
            if (watcher != null)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Changed -= OnFileChanged;
                watcher.Deleted -= OnFileDeleted;
            }
        }

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            if (!UnitTesting){
                MessageBox.Show($"Warning the file {e.Name} has changed");
            }
                
            FileChanged = true;
        }

        private void OnFileDeleted(object sender, FileSystemEventArgs e)
        {
            if (!UnitTesting){
                MessageBox.Show($"Warning the file {e.Name} has been deleted");
            }                

            FileDeleted = true;
        }
    }
}
