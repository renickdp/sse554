﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AccountSystem
{
    public class AccountDatabase
    {
        private List<Employee> employees;

        public AccountDatabase()
        {
            employees = new List<Employee>();
        }

        public List<Employee> Employees
        {
            get
            {
                return employees;
            }

            set
            {
                employees = value;
            }
        }

        public void Hire(string name, int age, double salary = 0.0)
        {
            employees.Add(new Employee(name, age, salary));
        }
    }
}
