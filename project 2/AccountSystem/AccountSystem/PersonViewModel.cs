﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountSystem
{
    public class PersonViewModel : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private int age;
        public int Age
        {
            get { return age; }
            set
            {
                age = value;
                OnPropertyChanged(nameof(Age));
            }
        }

        private DateTime addedDate;
        public DateTime AddedDate
        {
            get { return addedDate; }
            set
            {
                addedDate = value;
                OnPropertyChanged(nameof(AddedDate));
            }
        }
        
        public PersonViewModel()
        {
            Name = "John Doe";
            Age = 20;
            AddedDate = DateTime.Now;
        }

        public PersonViewModel(string name)
        {
            Name = name;
            Age = 20;
            AddedDate = DateTime.Now;
        }

        public PersonViewModel(string name, int age, DateTime addedData)
        {
            Name = name;
            Age = age;
            AddedDate = addedData;
        }

        public override string ToString()
        {
            return String.Format("{0} age {1} added {2}", Name, Age, AddedDate);
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
