﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountSystem
{
    public class CheckViewModel : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private double amount;
        public double Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                OnPropertyChanged(nameof(Amount));
            }
        }

        private string amountLabel;
        public string AmountLabel
        {
            get { return amountLabel; }
            set
            {
                amountLabel = value;
                OnPropertyChanged(nameof(AmountLabel));
            }
        }


        private string forLabel;
        public string ForLabel
        {
            get { return forLabel; }
            set
            {
                forLabel = value;
                OnPropertyChanged(nameof(ForLabel));
            }
        }

        private string date;
        public string Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged(nameof(Date));
            }
        }   

        public CheckViewModel(string name, double amount, string amountLabel, string forLabel, string date)
        {
            Name = name;
            Amount = amount;
            AmountLabel = amountLabel;
            ForLabel = forLabel;
            Date = date;
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
