﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Security.Claims;


namespace AccountSystem
{
    public class User
    {
        private string name;
        private bool isAdmin;

        private WindowsIdentity identity;
        private WindowsPrincipal principal;

        static private User instance = null;

        public string Name
        {
            get{ return name; }
            set{ name = value; }
        }

        public bool IsAdmin
        {
            get{ return isAdmin; }
            set{ isAdmin = value; }
        }

        public User()
        {
            identity = WindowsIdentity.GetCurrent();
            principal = new WindowsPrincipal(identity);

            name = identity.Name.Split('\\').LastOrDefault();
            isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public static User GetInstance()
        {
            if(instance == null)
            {
                instance = new User();
            }

            return instance;
        }
    }
}
