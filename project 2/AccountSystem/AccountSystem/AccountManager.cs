﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using System.Security.Cryptography;

namespace AccountSystem
{
    public class AccountManager : INotifyPropertyChanged
    {
        #region Binding fields
        private string guid;
        public string Guid
        {
            get { return guid; }
            set
            {
                guid = value;
                OnPropertyChanged(nameof(Guid));
            }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
            set
            {
                userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        private bool userAdmin;
        public bool UserAdmin
        {
            get { return userAdmin; }
            set
            {
                userAdmin = value;
                OnPropertyChanged(nameof(UserAdmin));
            }
        }

        private DateTime currentTime;
        public DateTime CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                OnPropertyChanged(nameof(CurrentTime));
            }
        }

        private string entryName;
        public string EntryName
        {
            get { return entryName; }
            set
            {
                entryName = value;
                OnPropertyChanged(nameof(EntryName));
            }
        }

        private int entryAge;
        public int EntryAge
        {
            get { return entryAge; }
            set
            {
                entryAge = value;
                OnPropertyChanged(nameof(EntryAge));
            }
        }

        private double entrySalary;
        public double EntrySalary
        {
            get { return entrySalary; }
            set
            {
                entrySalary = value;
                OnPropertyChanged(nameof(EntrySalary));
            }
        }

        private double hours;
        public double Hours
        {
            get { return hours; }
            set
            {
                hours = value;
                OnPropertyChanged(nameof(Hours));
            }
        }

        private string entryMonth;
        public string EntryMonth
        {
            get { return entryMonth; }
            set
            {
                entryMonth = value;
                OnPropertyChanged(nameof(EntryMonth));
            }
        }

        private string entryDay;
        public string EntryDay
        {
            get { return entryDay; }
            set
            {
                entryDay = value;
                OnPropertyChanged(nameof(EntryDay));
            }
        }

        private string entryYear;
        public string EntryYear
        {
            get { return entryYear; }
            set
            {
                entryYear = value;
                OnPropertyChanged(nameof(EntryYear));
            }
        }

        private string searchText = "";
        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged(nameof(SearchText));
            }
        }

        private string watchText;
        public string WatchText
        {
            get { return watchText; }
            set
            {
                watchText = value;
                OnPropertyChanged(nameof(WatchText));
            }
        }

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set
            {
                addCommand = value;
                OnPropertyChanged(nameof(AddCommand));
            }
        }

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set
            {
                searchCommand = value;
                OnPropertyChanged(nameof(SearchCommand));
            }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set
            {
                saveCommand = value;
                OnPropertyChanged(nameof(SaveCommand));
            }
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get { return openCommand; }
            set
            {
                openCommand = value;
                OnPropertyChanged(nameof(OpenCommand));
            }
        }

        private ICommand openEncryptedCommand;
        public ICommand OpenEncryptedCommand
        {
            get { return openEncryptedCommand; }
            set
            {
                openEncryptedCommand = value;
                OnPropertyChanged(nameof(OpenEncryptedCommand));
            }
        }

        private ICommand saveEncryptedCommand;
        public ICommand SaveEncryptedCommand
        {
            get { return saveEncryptedCommand; }
            set
            {
                saveEncryptedCommand = value;
                OnPropertyChanged(nameof(SaveEncryptedCommand));
            }
        }

        private ICommand exitCommand;
        public ICommand ExitCommand
        {
            get { return exitCommand; }
            set
            {
                exitCommand = value;
                OnPropertyChanged(nameof(ExitCommand));
            }
        }

        private ICommand watchCommand;
        public ICommand WatchCommand
        {
            get { return watchCommand; }
            set
            {
                watchCommand = value;
                OnPropertyChanged(nameof(WatchCommand));
            }
        }

        private ICommand checkRetrieveCommand;
        public ICommand CheckRetrieveCommand
        {
            get { return checkRetrieveCommand; }
            set
            {
                checkRetrieveCommand = value;
                OnPropertyChanged(nameof(CheckRetrieveCommand));
            }
        }

        private ICommand loadSampleCommand;
        public ICommand LoadSampleCommand
        {
            get { return loadSampleCommand; }
            set
            {
                loadSampleCommand = value;
                OnPropertyChanged(nameof(LoadSampleCommand));
            }
        }

        // This controls what shows up in the lookup search box
        private ObservableCollection<EmployeeViewModel> searchResults;
        public ObservableCollection<EmployeeViewModel> SearchResults
        {
            get { return searchResults; }
            set
            {
                searchResults = value;
                OnPropertyChanged(nameof(SearchResults));
            }
        }

        // This is the master list of employees that the UI knows of
        private ObservableCollection<EmployeeViewModel> employeeList;
        public ObservableCollection<EmployeeViewModel> EmployeeList
        {
            get { return employeeList; }
            set
            {
                employeeList = value;
                OnPropertyChanged(nameof(EmployeeList));
            }
        }

        // This is list which shows up in the pay period
        private ObservableCollection<string> payPeriodList;
        public ObservableCollection<string> PayPeriodList
        { 
            get { return payPeriodList; }
            set
            {
                payPeriodList = value;
                OnPropertyChanged(nameof(PayPeriodList));
            }
        }

        private int empolyeeCheckIndex;
        public int EmpolyeeCheckIndex
        {
            get { return empolyeeCheckIndex; }
            set
            {
                empolyeeCheckIndex = value;
                OnPropertyChanged(nameof(EmpolyeeCheckIndex));
            }
        }

        private int payPeriodIndex;
        public int PayPeriodIndex
        {
            get { return payPeriodIndex; }
            set
            {
                payPeriodIndex = value;
                OnPropertyChanged(nameof(PayPeriodIndex));
            }
        }

        // This is bound to the check property fields modify this to update check
        private CheckViewModel currentCheck;
        public CheckViewModel CurrentCheck
        {
            get { return currentCheck; }
            set
            {
                currentCheck = value;
                OnPropertyChanged(nameof(CurrentCheck));
            }
        }

        private bool searchByName;
        public bool SearchByName
        {
            get { return searchByName; }
            set
            {
                searchByName = value;
                OnPropertyChanged(nameof(SearchByName));
            }
        }

        private bool searchByAge;
        public bool SearchByAge
        {
            get { return searchByAge; }
            set
            {
                searchByAge = value;
                OnPropertyChanged(nameof(SearchByAge));
            }
        }

        private bool searchByDate;
        public bool SearchByDate
        {
            get { return searchByDate; }
            set
            {
                searchByDate = value;
                OnPropertyChanged(nameof(SearchByDate));
            }
        }

        #endregion

        private User currentUser;
        private DatabaseWatcher customWatcher;
        private string databasePath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AccountManager\";
        private AccountDatabase database;
        private XmlSerializer serializer;
        private Encrypter encrypter;

        public AccountManager()
        {
            AccountSystemEventSource.Log.ProgramStart();
            currentUser = User.GetInstance();
            CurrentTime = DateTime.Now;
            UserName = currentUser.Name;
            UserAdmin = currentUser.IsAdmin;
            EntryName = "Employee Name";
            EntryAge = 0;
            EntrySalary = 10.25;
            Hours = 80;
            EntryMonth ="MM";
            EntryDay = "DD";
            EntryYear = "YYYY";
            SearchByName = false;
            SearchByAge = false;
            SearchByDate = false;
            Guid = AccountSystemEventSource.Log.Guid.ToString();

            database = new AccountDatabase();
            customWatcher = new DatabaseWatcher(databasePath);
            serializer = new XmlSerializer(typeof(AccountDatabase));
            encrypter = new Encrypter();

            AddCommand = new SimpleCommand(_ => addEntry());
            SearchCommand = new SimpleCommand(_ => search(searchText));
            SaveCommand = new SimpleCommand(_ => saveDatabase(databasePath));
            SaveEncryptedCommand = new SimpleCommand(_ => saveEncryptedDatabase(databasePath));
            OpenCommand = new SimpleCommand(_ => loadDatabase(databasePath));
            OpenEncryptedCommand = new SimpleCommand(_ => loadEncryptedDatabase(databasePath));
            LoadSampleCommand = new SimpleCommand(_ => PopulateDatabase());
            ExitCommand = new SimpleCommand(_ => Application.Current.Shutdown());
            WatchCommand = new SimpleCommand(_ => StartWatchingFiles());
            EmpolyeeCheckIndex = 0;
            PayPeriodIndex = 0;
            WatchText = "Start Watching";

            SearchResults = new ObservableCollection<EmployeeViewModel>();
            employeeList = new ObservableCollection<EmployeeViewModel>();
            payPeriodList = new ObservableCollection<string>();
            CurrentCheck = new CheckViewModel("Test name", 121.0, "Test Amount Label", "Test For Label", "10/10/10");

            AccountSystemEventSource.Log.UiInitialized();
        }

        public void search(string searchText)
        {
            searchResults.Clear();

            foreach(EmployeeViewModel e in employeeList)
            {
                //search by name
                if (e.Name.ToLower().Contains(searchText.ToLower()))
                {
                    searchResults.Add(e);
                    SearchByName = true;
                    SearchByAge = false;
                    SearchByDate = false;
                }
                //search by age
                else if((searchText.ToLower().Contains("age") || searchText.ToLower().Contains("years old") || searchText.ToLower().Contains("year old")) && searchText.Contains(e.Age.ToString()))
                {
                    searchResults.Add(e);
                    SearchByName = false;
                    SearchByAge = true;
                    SearchByDate = false;
                }
                //search by added date
                else if (searchText.ToLower().Contains("added") && (searchText.ToLower().Contains(e.AddedDate.DayOfWeek.ToString().ToLower()) || searchText.ToLower().Contains(e.AddedDate.Day.ToString().ToLower())))
                {
                    searchResults.Add(e);
                    SearchByName = false;
                    SearchByAge = false;
                    SearchByDate = true;
                }
            }
        }

        public void addEntry()
        {
            DateTime date;
            if (!DateTime.TryParse(String.Format("{1}/{2}/{0}", EntryYear, EntryMonth, EntryDay), out date))
            {
                MessageBox.Show("Error: Invalid pay date entered");
                return;
            }

            var employee = database.Employees.Find(e => e.Age == EntryAge && e.Name == EntryName);
            if(employee == null)
            {
                employee = new Employee(EntryName, EntryAge, EntrySalary, DateTime.Now);
                database.Employees.Add(employee);
            }
            
            employee.Pay(Hours, date);

            UpdateEmployeeList();
            MessageBox.Show("The data was added successfully ");

        }

        public void loadDatabase(string path)
        {
            var test = new DirectoryInfo(path);

            if (test.Exists)
            {
                // Read the file
                FileStream fileStream = new FileStream(path + "db.xml", FileMode.Open);
                database = (AccountDatabase)serializer.Deserialize(fileStream);
                fileStream.Close();

                UpdateEmployeeList();
            }
        }

        public void saveDatabase(string path)
        {
            //if the specified path does not exist, create it
            var test = new DirectoryInfo(path);
            if (!test.Exists) Directory.CreateDirectory(path);

                //Serialize then save this object to a file
                StreamWriter writer = new StreamWriter(path + "db.xml");
                serializer.Serialize(writer, database);
                writer.Close();
        }

        public void loadEncryptedDatabase(string path)
        {


            var test = new DirectoryInfo(path);

            if (test.Exists)
            {
                using (var fs = new FileStream(path + "db.bin", FileMode.Open))
                {
                    using (var cs = encrypter.GetDecrypter(fs))
                    {
                        XmlSerializer xmlser = new XmlSerializer(typeof(AccountDatabase));
                        database = (AccountDatabase)xmlser.Deserialize(cs);
                    }
                }

                UpdateEmployeeList();

            }
        }

        public void saveEncryptedDatabase(string path)
        {
            //if the specified path does not exist, create it
            var test = new DirectoryInfo(path);
            if (!test.Exists) Directory.CreateDirectory(path);

            using (var fs = new FileStream(path + "db.bin", FileMode.Create))
            {
                using (var cs = encrypter.GetEncrypter(fs))
                {
                    XmlSerializer xmlser = new XmlSerializer(typeof(AccountDatabase));
                    xmlser.Serialize(cs, database);
                }
            }
        }

        public void UpdateEmployeeList()
        {
            SearchResults.Clear();
            EmployeeList.Clear();

            foreach (Employee p in database.Employees)
            {
                EmployeeList.Add(new EmployeeViewModel(p));
                SearchResults.Add(new EmployeeViewModel(p));
            }
        }

        public void UpdateCheck()
        {
            int i = EmpolyeeCheckIndex;
            int eLimit = database.Employees.Count;

            if (i < eLimit && i >= 0)
            {
                int j = PayPeriodIndex;
                int pLimit = database.Employees[i].Paychecks.Count;

                if (j < PayPeriodList.Count() && j >= 0)
                {
                    CurrentCheck.Name = database.Employees[i].Name;
                    CurrentCheck.Date = database.Employees[i].Paychecks[j].Date.ToString("MM-dd-yyyy");
                    CurrentCheck.Amount = database.Employees[i].Paychecks[j].Amount * database.Employees[i].Salary;
                    CurrentCheck.ForLabel = "Salary for " + database.Employees[i].Paychecks[j].Amount + " hours";
                    currentCheck.AmountLabel = NumberToWords.Convert(Convert.ToInt32(currentCheck.Amount));
                }
            }
        }

        public void UpdatePayPeriodList()
        {
            if(EmployeeList.Count > 0)
            {
                var vmEmployee = EmployeeList[EmpolyeeCheckIndex];
                var employee = database.Employees.Where(e => e.Name == vmEmployee.Name && e.Age == vmEmployee.Age).FirstOrDefault();

                payPeriodList.Clear();
                foreach (Paycheck p in employee.Paychecks)
                    PayPeriodList.Add(p.Date.ToString());
            }     
        }

        private void StartWatchingFiles()
        {
            AccountSystemEventSource.Log.FileWatcherStart();
            customWatcher?.BeginWatching();
            WatchCommand = new SimpleCommand(_ => StopWatchingFiles());
            WatchText = "Stop Watching";
        }

        public void StopWatchingFiles()
        {
            AccountSystemEventSource.Log.FileWatcherStart();
            customWatcher?.StopWatching();
            WatchCommand = new SimpleCommand(_ => StartWatchingFiles());
            WatchText = "Start Watching";
        }

        #region Data popluation for debugging
        private void PopulateDatabase()
        {
            database.Employees.Clear();
            database.Hire("Person 1", 20, 7.25);
            database.Hire("Person 2", 30);
            database.Hire("Person 3", 40);
            database.Hire("Person 4", 50);
            database.Hire("Person 5", 60);
            database.Hire("Person 6", 70);

            foreach(Employee e in database.Employees)
            {
                for(int i = 0; i < 5; i++)
                {
                    var time = DateTime.Now.AddMonths(i);
                    var salary = e.Salary * 80;
                    e.Pay(salary, time);
                }               
            }

            UpdateEmployeeList();
        }
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
