﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void UserTest_Constructor()
        {
            var expectedName = "Michael"; // Will change depending on user
            var expectedAdmin = false; // Will change if run as administrator
            var u = new User();

            Assert.AreEqual(expectedName, u.Name);
            Assert.AreEqual(expectedAdmin, u.IsAdmin);
        }

        [TestMethod]
        public void UserTest_GetInstance()
        {
            var u = User.GetInstance();
            var u2 = User.GetInstance();

            Assert.AreEqual(u, u2);
        }
    }
}
