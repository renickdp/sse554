﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;
using System.ComponentModel;

namespace AccountSystemTests
{
    [TestClass]
    public class CheckViewModelTests
    {
        [TestMethod]
        public void CheckViewModelTest_Constructor()
        {
            var cvm = new CheckViewModel("Test name", 100, "Test amount label", "Test for label", "Test date");

            Assert.AreEqual("Test name", cvm.Name);
            Assert.AreEqual(100, cvm.Amount);
            Assert.AreEqual("Test amount label", cvm.AmountLabel);
            Assert.AreEqual("Test for label", cvm.ForLabel);
            Assert.AreEqual("Test date", cvm.Date);
        }

        [TestMethod]
        public void CheckViewModelTest_NameProperty()
        {
            var th = new TestHelper("Name");
            var cvm = new CheckViewModel("Test name", 100, "Test amount label", "Test for label", "Test date");
            cvm.PropertyChanged += th.Handler;
            cvm.Name = "Different Name";
            Assert.AreEqual(true, th.EventTriggered);
        }

        [TestMethod]
        public void CheckViewModelTest_AmountProperty()
        {
            var th = new TestHelper("Amount");
            var cvm = new CheckViewModel("Test name", 100, "Test amount label", "Test for label", "Test date");
            cvm.PropertyChanged += th.Handler;
            cvm.Amount = 50;
            Assert.AreEqual(true, th.EventTriggered);
        }

        [TestMethod]
        public void CheckViewModelTest_AmountLabelProperty()
        {
            var th = new TestHelper("AmountLabel");
            var cvm = new CheckViewModel("Test name", 100, "Test amount label", "Test for label", "Test date");
            cvm.PropertyChanged += th.Handler;
            cvm.AmountLabel = "Different amount label";
            Assert.AreEqual(true, th.EventTriggered);
        }

        [TestMethod]
        public void CheckViewModelTest_ForLabelProperty()
        {
            var th = new TestHelper("ForLabel");
            var cvm = new CheckViewModel("Test name", 100, "Test amount label", "Test for label", "Test date");
            cvm.PropertyChanged += th.Handler;
            cvm.ForLabel = "Different for label";
            Assert.AreEqual(true, th.EventTriggered);
        }

        [TestMethod]
        public void CheckViewModelTest_DateProperty()
        {
            var th = new TestHelper("Date");
            var cvm = new CheckViewModel("Test name", 100, "Test amount label", "Test for label", "Test date");
            cvm.PropertyChanged += th.Handler;
            cvm.Date = "Different date";
            Assert.AreEqual(true, th.EventTriggered);
        }
    }

    class TestHelper
    {
        private string watchString;
        public bool EventTriggered;

        public TestHelper(string watchString)
        {
            this.watchString = watchString;
            EventTriggered = false;
        }

        public void Handler(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == watchString)
                EventTriggered = true;
        }
    }
}
