﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;
using System.IO;
using System.Linq;

namespace AccountSystemTests
{
    [TestClass]
    public class EncrypterTests
    {
        [TestMethod]
        public void EncrypterTest_GetEncrypter()
        {
            var e = new Encrypter();
            byte[] dataIn;
            byte[] dataOut;
            List<int> data = new List<int>();

            for(int i = 0; i < 50; i++){ data.Add(i); }

            int[] dataArray = data.ToArray();
            dataIn = new byte[dataArray.Length * sizeof(int)];
            Buffer.BlockCopy(dataArray, 0, dataIn, 0, dataIn.Length);

            using (var ms = new MemoryStream())
            {
                using (var cs = e.GetEncrypter(ms))
                {
                    cs.Write(dataIn, 0, dataIn.Length);
                }
                dataOut = ms.ToArray();
            }

            var x = dataIn.SequenceEqual(dataOut);
            Assert.AreEqual(false, x);  
        }

        [TestMethod]
        public void EncrypterTest_GetDecrypter()
        {
            var e = new Encrypter();
            byte[] dataIn;
            byte[] dataOut;
            List<int> data = new List<int>();

            for (int i = 0; i < 50; i++) { data.Add(i); }

            int[] dataArray = data.ToArray();
            dataIn = new byte[dataArray.Length * sizeof(int)];
            dataOut = new byte[dataArray.Length * sizeof(int)];
            Buffer.BlockCopy(dataArray, 0, dataIn, 0, dataIn.Length);

            byte[] encryptedData;
            using (var ms = new MemoryStream())
            {
                using (var cs = e.GetEncrypter(ms))
                {
                    cs.Write(dataIn, 0, dataIn.Length);
                }
                encryptedData = ms.ToArray();
            }

            using (var ms = new MemoryStream(encryptedData))
            {
                using (var cs = e.GetDecrypter(ms))
                {
                    int readData;
                    for(int i = 0; i < dataOut.Length && ((readData = cs.ReadByte()) != -1); i++)
                    {
                        dataOut[i] = (byte)readData;
                    }
                }
            }

            var x = dataIn.SequenceEqual(dataOut);
            Assert.AreEqual(true, x);
        }
    }
}
