﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
    [TestClass]
    public class AccountDatabaseTests
    {
        [TestMethod]
        public void AccountDatabaseTest_Constructor()
        {
            var adb = new AccountDatabase();

            Assert.AreEqual(0, adb.Employees.Count);
        }

        [TestMethod]
        public void AccountDatabaseTest_Hire()
        {
            var adb = new AccountDatabase();
            adb.Hire("Test Name", 20, 10.0);

            Assert.AreEqual(1, adb.Employees.Count);
            Assert.AreEqual("Test Name", adb.Employees[0].Name);
            Assert.AreEqual(20, adb.Employees[0].Age);
            Assert.AreEqual(10.0, adb.Employees[0].Salary);
        }
    }
}
