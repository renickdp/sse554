﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
    [TestClass]
    public class SimpleCommandTests
    {
        private bool executed;
        private bool canExecuteChanged;

        private void helperDelegate(object o)
        {
            executed = true;
        }

        private void helperHandler(object sender, EventArgs e)
        {
            canExecuteChanged = true;
        }

        [TestMethod]
        public void CommandTest_CanExecute1()
        {
            int i = 0;
            var c = new SimpleCommand(helperDelegate);

            Assert.AreEqual(true, c.CanExecute(i));
        }

        [TestMethod]
        public void CommandTest_CanExecute2()
        {
            int i = 0;
            var c = new SimpleCommand(null);

            Assert.AreEqual(false, c.CanExecute(i));
        }

        [TestMethod]
        public void CommandTest_Execute()
        {
            int i = 0;
            var c = new SimpleCommand(helperDelegate);

            executed = false;

            c.Execute(i);

            Assert.AreEqual(true, executed);
        }

        [TestMethod]
        public void CommandTest_OnExecute()
        {
            int i = 0;
            var c = new SimpleCommand(helperDelegate);
            c.CanExecuteChanged += helperHandler;

            canExecuteChanged = false;

            c.Execute(i);

            Assert.AreEqual(true, canExecuteChanged);
        }
    }
}
