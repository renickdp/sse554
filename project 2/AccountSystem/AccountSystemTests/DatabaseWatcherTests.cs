﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
    [TestClass]
    public class DatabaseWatcherTests
    {
        [TestMethod]
        public void DatabaseWatcherTest_FileChanged()
        {
            var path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var dbw = new DatabaseWatcher(path);
            string[] text = { "text" };

            Assert.AreEqual(false, dbw.FileChanged);
            
            dbw.UnitTesting = true;
            dbw.BeginWatching();
            
            System.IO.File.WriteAllLines(path + @"\temp.txt", text);
            System.Threading.Thread.Sleep(10);

            Assert.AreEqual(true, dbw.FileChanged);
        }

        [TestMethod]
        public void DatabaseWatcherTest_FileDeletedChanged()
        {
            var path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var dbw = new DatabaseWatcher(path);
            string[] text = { "text" };

            System.IO.File.WriteAllLines(path + @"\temp.txt", text);

            Assert.AreEqual(false, dbw.FileDeleted);

            dbw.UnitTesting = true;
            dbw.BeginWatching();

            System.IO.File.Delete(path + @"\temp.txt");
            System.Threading.Thread.Sleep(10);

            Assert.AreEqual(true, dbw.FileDeleted);
        }
    }
}
