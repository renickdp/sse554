﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
[TestClass]
public class EmployeeViewModelTests
{
    [TestMethod]
    public void EmployeeViewModelTest_Constructor()
    {
        var date = DateTime.Now;
        var evm = new EmployeeViewModel("Test name", 20, date);

        Assert.AreEqual("Test name", evm.Name);
        Assert.AreEqual(20, evm.Age);
        Assert.AreEqual(date, evm.AddedDate);
    }

    [TestMethod]
    public void EmployeeViewModelTest_NameProperty()
    {
        var th = new TestHelper("Name");
        var evm = new EmployeeViewModel("Test name");
        evm.PropertyChanged += th.Handler;
        evm.Name = "Different Name";
        Assert.AreEqual(true, th.EventTriggered);
    }

    [TestMethod]
    public void EmployeeViewModelTest_AgeProperty()
    {
        var th = new TestHelper("Age");
        var evm = new EmployeeViewModel("Test name");
        evm.PropertyChanged += th.Handler;
        evm.Age = 50;
        Assert.AreEqual(true, th.EventTriggered);
    }

    [TestMethod]
    public void EmployeeViewModelTest_AddedDateProperty()
    {
        var date = DateTime.Now;
        var th = new TestHelper("AddedDate");
        var evm = new EmployeeViewModel("Test name");
        evm.PropertyChanged += th.Handler;
        evm.AddedDate = date;
        Assert.AreEqual(true, th.EventTriggered);
    }
}
}
