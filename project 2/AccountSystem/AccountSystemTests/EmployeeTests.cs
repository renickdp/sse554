﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
    [TestClass]
    public class EmployeeTests
    {
        [TestMethod]
        public void EmployeeTest_Constructor()
        {
            var date = DateTime.Now;
            var e = new Employee("Test Name", 10, date);

            Assert.AreEqual("Test Name", e.Name);
            Assert.AreEqual(10, e.Age);
            Assert.AreEqual(date, e.JoinDate);
        }

        [TestMethod]
        public void EmployeeTest_Pay()
        {
            var date = DateTime.Now;
            var e = new Employee("Test Name", 10);
            e.Pay(100, date);

            Assert.AreEqual(1, e.Paychecks.Count);
            Assert.AreEqual(100, e.Paychecks[0].Amount);
            Assert.AreEqual(date, e.Paychecks[0].Date);
        }
    }
}
