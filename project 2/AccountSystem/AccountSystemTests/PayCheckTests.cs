﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;

namespace AccountSystemTests
{
    [TestClass]
    public class PayCheckTests
    {
        [TestMethod]
        public void PayCheckTest_Constructor1()
        {
            var date = DateTime.Now;
            var pc = new Paycheck();

            Assert.AreEqual(0, pc.Amount);
            Assert.AreEqual(date, pc.Date);
        }

        [TestMethod]
        public void PayCheckTest_Constructor2()
        {
            var date = DateTime.Now;
            var pc = new Paycheck(10);

            Assert.AreEqual(10, pc.Amount);
            Assert.AreEqual(date, pc.Date);
        }

        [TestMethod]
        public void PayCheckTest_Constructor3()
        {
            var date = DateTime.Now;
            var pc = new Paycheck(20, date);

            Assert.AreEqual(20, pc.Amount);
            Assert.AreEqual(date, pc.Date);
        }
    }
}
