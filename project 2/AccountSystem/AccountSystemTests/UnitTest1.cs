﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSystem;


namespace AccountSystemTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConverterGood()
        {
            Assert.AreEqual("three hundred and twentyfive", NumberToWords.Convert(325));
        }

        [TestMethod]
        public void TestConverterBad()
        {
            Assert.AreEqual("onehundred", NumberToWords.Convert(200));
        }

        [TestMethod]
        public void TestSearchEmptyString()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("");
            Assert.AreEqual(3, am.SearchResults.Count);
        }

        [TestMethod]
        public void TestSearchNoResults()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("4w4wrg");
            Assert.AreEqual(0, am.SearchResults.Count);
        }

        [TestMethod]
        public void TestSearchByUniqueAndPartialName()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("Person O");
            Assert.AreEqual(1, am.SearchResults.Count);
        }

        [TestMethod]
        public void TestSearchByCommonName()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("Person");
            Assert.AreEqual(3, am.SearchResults.Count);
        }

        [TestMethod]
        public void TestSearchCaseInsensitive()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("perSON");
            Assert.AreEqual(3, am.SearchResults.Count);
        }

        [TestMethod]
        public void TestSearchByAge()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("30 year old employees");
            Assert.AreEqual(1, am.SearchResults.Count);
        }

        [TestMethod]
        public void TestSearchByAddedDate()
        {
            AccountManager am = new AccountManager();
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person One", 30, DateTime.Now)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Two", 21, DateTime.MinValue)));
            am.EmployeeList.Add(new EmployeeViewModel(new Employee("Person Three", 50, DateTime.MaxValue)));
            am.search("employees added January 1, 0001");
            Assert.AreEqual(1, am.SearchResults.Count);
        }
    }
}
