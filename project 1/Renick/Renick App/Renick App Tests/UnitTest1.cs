﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Renick_App;

namespace Renick_App_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GrossweightGood()
        {
            Assert.AreEqual(500000, fuelservice.GetgrossWeight(380000, 120000));
        }

        [TestMethod]
        public void GrossweightBad()
        {
            Assert.AreEqual(600002, fuelservice.GetgrossWeight(500000, 100000));

        }

        [TestMethod]
        public void TempConversionGood()
        {
            Assert.AreEqual(98.6, WeatherService.CelciustoFahrenheit(37));
        }

        [TestMethod]
        public void TestTempConversionBad()
        {
            Assert.AreEqual(97.6, WeatherService.CelciustoFahrenheit(37));
        }

        [TestMethod]
        public void PressureAltitudeGood()
        {
            Assert.AreEqual(1470, Convert.ToInt32(WeatherService.PressureAltitude(1000)));
        }

        [TestMethod]
        public void PressureAltitudeBad()
        {
            Assert.AreEqual(500, Convert.ToInt32(WeatherService.PressureAltitude(37)));
        }

        [TestMethod]
        public void DensityAltitudeGood()
        {
            Assert.AreEqual(870, Convert.ToInt32(WeatherService.DensityAltitude(1470, 45)));
        }

        [TestMethod]
        public void DensityAltitudeBad()
        {
            Assert.AreEqual(563.6, Convert.ToInt32(WeatherService.DensityAltitude(37, 40)));
        }

    }
}
