﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Renick_App
{
    public partial class Aircraft : Form
    {
        public Aircraft()
        {
            InitializeComponent();
            pictureBox1.Image = Image.FromFile("img/c5.jpg");

        }

        private void CalculateBtn_Click(object sender, EventArgs e)
        {

            var oktoCalculate = Checkfields(false);
            if (oktoCalculate == true)
            {
            //call calculate gross weight class        
            var result = fuelservice.GetgrossWeight(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text));
            outputweights.Text = Convert.ToString(result);


            //call calculate temperature class
            TempOutput.Text = Convert.ToString(WeatherService.CelciustoFahrenheit(Convert.ToDouble(tempTextbox.Text)));

           //call calculate pressure altitude
           PressureAltitudeOutput.Text = Convert.ToString(WeatherService.PressureAltitude
               (Convert.ToDouble(FieldElevationtextBox.Text)));

            //call density altitude
            DensityOutput.Text = Convert.ToString(WeatherService.DensityAltitude
                (Convert.ToDouble(PressureAltitudeOutput.Text), Convert.ToDouble(tempTextbox.Text)));

             }

        }

        public bool Checkfields(bool oktoCalculate)
        {
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Please enter data for the fuel weights");
                return oktoCalculate = false;
            }
            else if (string.IsNullOrEmpty(tempTextbox.Text))
            {
                MessageBox.Show("Please enter the temperature value");
                return oktoCalculate = false;
            }
            else
            {
               return oktoCalculate = true;
            }
        }



    }
}
