﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renick_App
{
     public class WeatherService
    {
        public static double CelciustoFahrenheit(double temp)
        {
            var x = ((temp * 9/5) + 32);
            return x;
        }

        public static double PressureAltitude(double FieldElevation)
        {
            var PAlt = (29.92 - 29.45) * 1000 + FieldElevation;
            return PAlt;
        }

        public static double DensityAltitude(double Palt, double temp)
        {
            var DAlt = Palt + (120 * (temp - (temp -(-5))));
            return DAlt;
        }

    }
}
