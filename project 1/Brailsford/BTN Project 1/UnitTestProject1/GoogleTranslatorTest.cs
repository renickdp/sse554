﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BTN_Project_1;
using System.Threading;

namespace UnitTestProject1
{
    [TestClass]
    public class GoogleTranslatorTest
    {
        [TestMethod]
        public void TestNorwegianToSpanish()
        {
            GoogleTranslator gt = new GoogleTranslator();
            Assert.AreEqual(gt.translateText("hund", "Norwegian", "Italian"), "https://translate.google.com/#no/it/hund");
        }

        [TestMethod]
        public void TestEmptyStrings()
        {
            GoogleTranslator gt = new GoogleTranslator();
            Assert.AreEqual(gt.translateText("", "", ""), "https://translate.google.com/#//");
        }
        [TestMethod]
        public void TestAlternateSymbols()
        {
            GoogleTranslator gt = new GoogleTranslator();
            Assert.AreEqual(gt.translateText("áÀéÈíÌóÒúÙñüæÆäÄøØöÖåÅ", "English", "Spanish"), "https://translate.google.com/#en/es/áÀéÈíÌóÒúÙñüæÆäÄøØöÖåÅ");
        }
        [TestMethod]
        public void TestReatimeEntry()
        {
            GoogleTranslator gt = new GoogleTranslator();
            Assert.AreEqual(gt.translateText("h", "English", "Italian"), "https://translate.google.com/#en/it/h");
            Assert.AreEqual(gt.translateText("ho", "English", "Italian"), "https://translate.google.com/#en/it/ho");
            Assert.AreEqual(gt.translateText("hou", "English", "Italian"), "https://translate.google.com/#en/it/hou");
            Assert.AreEqual(gt.translateText("houn", "English", "Italian"), "https://translate.google.com/#en/it/houn");
            Assert.AreEqual(gt.translateText("hound", "English", "Italian"), "https://translate.google.com/#en/it/hound");
        }
    }
}
