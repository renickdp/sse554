﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTN_Project_1
{
    public partial class GoogleTranslator : Form
    {
        //Create a timer used to provide a small delay between the page loading and the output being set
        public System.Timers.Timer delayTimer = new System.Timers.Timer(400);

        public GoogleTranslator()
        {
            InitializeComponent();
            //attach custom timer event handler to delayTimer
            delayTimer.Elapsed += delayedOutputRequest;

        }

        //convert languages to shorthand
        public string shortenLanguage(string s)
        {
            switch (s)
            {
                case "English":
                    return "en";
                case "Spanish":
                    return "es";
                case "Italian":
                    return "it";
                case "Norwegian":
                    return "no";
                default: return null;
            }
        }

        //main translation function
        public string translateText(string text, string inputLanguage, string outputLanguage)
        {
            //convert input and output strings into a format google translate will accept
            inputLanguage = shortenLanguage(inputLanguage);
            outputLanguage = shortenLanguage(outputLanguage);
            string url = "https://translate.google.com/#" + inputLanguage + "/" + outputLanguage + "/" + text;

            //disable WebBrowser so it does not steal focus from the input field
            WebBrowser.Parent.Enabled = false;
            WebBrowser.Url = new Uri(url);
            //Send a blank request to update the page after text is translated
            WebBrowser.Url = new Uri(url);

            Console.WriteLine("Navigating to " + url);
            return url;
        }

        //encapsulated methods
        public ComboBox OutputComboBox
        {
            get
            {
                return outputComboBox;
            }

            set
            {
                outputComboBox = value;
            }
        }

        public TextBox InputTextBox
        {
            get
            {
                return inputTextBox;
            }

            set
            {
                inputTextBox = value;
            }
        }

        public ComboBox InputComboBox
        {
            get
            {
                return inputComboBox;
            }

            set
            {
                inputComboBox = value;
            }
        }
        public TextBox OutputTextBox
        {
            get
            {
                return outputTextBox;
            }

            set
            {
                outputTextBox = value;
            }
        }

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {
            translateText(inputTextBox.Text, inputComboBox.SelectedItem.ToString(), outputComboBox.SelectedItem.ToString());
        }

        private void inputComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            translateText(inputTextBox.Text, inputComboBox.SelectedItem.ToString(), outputComboBox.SelectedItem.ToString());
        }

        private void outputComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            translateText(inputTextBox.Text, inputComboBox.SelectedItem.ToString(), outputComboBox.SelectedItem.ToString());
        }

        //Because the pages does not create the translated text object until after it is loaded, a small delay must be added before setting the outputTextBox
        private void delayedOutputRequest(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (outputTextBox.InvokeRequired)
            {
                outputTextBox.Invoke(new MethodInvoker(delegate
                {
                    if (WebBrowser.Document.GetElementById("result_box").FirstChild != null)
                    {
                        outputTextBox.Clear();
                        //because the result_box may split the words into separate tags, combine all child tags of the result_box tag
                        for (int i = 0; i < WebBrowser.Document.GetElementById("result_box").Children.Count; i++)
                        {
                            outputTextBox.Text += " " + WebBrowser.Document.GetElementById("result_box").Children[i].InnerHtml;
                        }
                        
                    }
                }));
            }
            //disable the timer after a single delay interval
            delayTimer.Enabled = false;
        }

        private void WebBrowser_PageLoaded(object sender, EventArgs e)
        {


            delayTimer.Enabled = true;
            WebBrowser.Parent.Enabled = true;
        }
    }
}
