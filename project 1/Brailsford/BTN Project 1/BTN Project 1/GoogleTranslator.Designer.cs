﻿using System.Windows.Forms;

namespace BTN_Project_1
{
    partial class GoogleTranslator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputComboBox = new System.Windows.Forms.ComboBox();
            this.outputComboBox = new System.Windows.Forms.ComboBox();
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.WebBrowser = new System.Windows.Forms.WebBrowser();
            this.label1 = new System.Windows.Forms.Label();
            this.outputTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // inputComboBox
            // 
            this.inputComboBox.DropDownWidth = 140;
            this.inputComboBox.FormattingEnabled = true;
            this.inputComboBox.Items.AddRange(new object[] {
            "English",
            "Spanish",
            "Italian",
            "Norwegian"});
            this.inputComboBox.Location = new System.Drawing.Point(12, 3);
            this.inputComboBox.Name = "inputComboBox";
            this.inputComboBox.Size = new System.Drawing.Size(100, 21);
            this.inputComboBox.TabIndex = 0;
            this.inputComboBox.Text = "English";
            this.inputComboBox.SelectedIndexChanged += new System.EventHandler(this.inputComboBox_SelectedIndexChanged);
            // 
            // outputComboBox
            // 
            this.outputComboBox.DropDownWidth = 140;
            this.outputComboBox.FormattingEnabled = true;
            this.outputComboBox.Items.AddRange(new object[] {
            "English",
            "Spanish",
            "Italian",
            "Norwegian"});
            this.outputComboBox.Location = new System.Drawing.Point(212, 3);
            this.outputComboBox.Name = "outputComboBox";
            this.outputComboBox.Size = new System.Drawing.Size(100, 21);
            this.outputComboBox.TabIndex = 1;
            this.outputComboBox.Text = "English";
            this.outputComboBox.SelectedIndexChanged += new System.EventHandler(this.outputComboBox_SelectedIndexChanged);
            // 
            // inputTextBox
            // 
            this.inputTextBox.Location = new System.Drawing.Point(12, 27);
            this.inputTextBox.Multiline = true;
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(300, 100);
            this.inputTextBox.TabIndex = 2;
            this.inputTextBox.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            // 
            // WebBrowser
            // 
            this.WebBrowser.CausesValidation = false;
            this.WebBrowser.Location = new System.Drawing.Point(331, 3);
            this.WebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebBrowser.Name = "WebBrowser";
            this.WebBrowser.ScrollBarsEnabled = false;
            this.WebBrowser.Size = new System.Drawing.Size(659, 717);
            this.WebBrowser.TabIndex = 4;
            this.WebBrowser.Url = new System.Uri("https://translate.google.com/#en/en", System.UriKind.Absolute);
            this.WebBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.WebBrowser_PageLoaded);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "--To--";
            // 
            // outputTextBox
            // 
            this.outputTextBox.Location = new System.Drawing.Point(12, 133);
            this.outputTextBox.Multiline = true;
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.Size = new System.Drawing.Size(300, 100);
            this.outputTextBox.TabIndex = 6;
            // 
            // GoogleTranslator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 242);
            this.Controls.Add(this.outputTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.WebBrowser);
            this.Controls.Add(this.inputTextBox);
            this.Controls.Add(this.outputComboBox);
            this.Controls.Add(this.inputComboBox);
            this.Name = "GoogleTranslator";
            this.Text = "GoogleTranslator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox outputComboBox;
        private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.ComboBox inputComboBox;
        private WebBrowser WebBrowser;
        private Label label1;
        private TextBox outputTextBox;
    }
}

